/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        blue: "#4096ff",
        "rose-light": "#f6f7fb",
        purple: "#4257b2",
        "dark-purple": "#3e3ed7",
        "galaxy-purple": "#2d1674",
        cardfliptop: "#4a56b0",
        cardflipbottom: "#5794fc",
        pink: "#ff49db",
        orange: "#ff7849",
        green: "#43c361",
        yellow: "#f5e459",
        wheat: "rgba(255,234,193,1)",
        "gray-dark": "#3e4a5c",
        gray: "#939bb4",
        "gray-light": "#d3dce6",
        "green-light": "#e4ffe7",
        ground: "#333",
      },
      fontWeight: {
        bold: "600",
      },
      backgroundImage: {
        "header-banner": "url('images/header.png')",
      },
    },
  },
  plugins: [],
};
