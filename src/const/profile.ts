import { FolderViewOutlined, UserOutlined } from "@ant-design/icons";

export const menuProfile = [
  {
    key: "Sets",
    tile: "Sets",
    icon: "",
  },
  {
    key: "Members",
    tile: "Members ",
    icon: UserOutlined,
  },
  {
    key: "Folders",
    tile: "Folders",
    icon: FolderViewOutlined,
  },
];
