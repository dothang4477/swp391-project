import CollectionList from "components/Collections";
import { useEffect } from "react";

const CollectionsPage = () => {
  useEffect(()=>{
    document.title = `Collections | Quizfer`;
  })
  return <CollectionList />;
};

export default CollectionsPage;
