import { Outlet } from "react-router-dom";

const FlashCarSetPage = () => {
  return <div>
    <Outlet />
  </div>;
}

export default FlashCarSetPage;
