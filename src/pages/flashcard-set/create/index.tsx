import CreateFlashCardSet from "components/FlashCardSet/Create";
import { useEffect } from "react";

function CreateFlashCardSetPage() {
  useEffect(() => {
    document.title = `Create | Quizfer`;
  });
  return <CreateFlashCardSet />;
}

export default CreateFlashCardSetPage;
