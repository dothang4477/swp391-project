import { Tabs } from "antd";
import { FolderViewOutlined } from "@ant-design/icons";
import { menuProfile } from "const/profile";
import { useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import UserInfo from "components/UserProfile/Infomation";
import Search from "antd/es/input/Search";

const UserProfile = () => {
  const navigate = useNavigate();
  let { search } = useLocation();
  const query = new URLSearchParams(search);
  const [currentTab, setCurrentTab] = useState(query.get("tab") || "overview");

  // const renderContentByTab = useMemo(() => {
  //   switch (currentTab) {
  //     case "overview":
  //       return (
  //         <Search
  //           className="w-full h-[70px] border border-slate-200 rounded-md focus-within:shadow-md focus-within:border-none"
  //           placeholder="Input cho bạn Hiếu"
  //           allowClear
  //         />
  //       );
  //     case "demo1":
  //       return <>Demo 1</>;
  //     case "demo2":
  //       return <>Demo 2</>;
  //   }
  // }, [currentTab]);

  return (
    <div className="container mx-auto flex gap-20">
      <UserInfo />
      {/* <Tabs
        defaultActiveKey={currentTab}
        size="large"
        className="w-full text-xl"
        onChange={(tab) => {
          navigate(`?tab=${tab}`);
          setCurrentTab(tab);
        }}
        items={menuProfile.map((item) => {
          return {
            label: (
              <span className="text-lg">
                <FolderViewOutlined />
                <span className="mb-0">{item.tile}</span>
              </span>
            ),
            key: item.key,
            children: renderContentByTab,
          };
        })}
      /> */}
    </div>
  );
};

export default UserProfile;
