import React, { useContext, useState } from "react";
import { listCard } from "mock";
import FlashCardSetInfo from "components/FlashCardSet/CardInfo";
import ButtonQuizfer from "components/ui/button";
import {
  CheckCircleOutlined,
  RedoOutlined,
  RiseOutlined,
} from "@ant-design/icons";
import "./style.scss";
import { Carousel, Modal } from "antd";
import InputQuizfer from "components/ui/input";
import { useNavigate } from "react-router-dom";
// import SocketQuizferWrapper from "config/socket-io";
import useSocket from "hooks/socketFunc";
import { Else, If, Then } from "react-if";
import { useAppSelector } from "reduxHook";
import { AuthContext } from "config/context/auth";
const ads1 = require("images/Advertize-1.png");
const ads2 = require("images/Advertize-2.png");
const logged = require("images/logged.png");
const joinQuizFer = require("images/joinquizfer.gif");

const HomePage = () => {
  const navigate = useNavigate();
  const [roomCode, setRoomCode] = useState("");
  const [isOpenEnterRoomCode, setIsOpenEnterRoomCode] =
    useState<boolean>(false);
  const [refSocket, setRefSocket] = useState<any>(null);
  const { sendMessageSocket } = useSocket(refSocket);
  const { isAuthenticated } = useContext(AuthContext);

  const goToGame = () => {
    // sendMessageSocket({
    //   to: [roomCode],
    //   message: "newJoin",
    //   value: {
    //     userInfo: {
    //       id: "ducnh",
    //       name: "Ducnh",
    //       avatar:
    //         "https://scontent.xx.fbcdn.net/v/t39.30808-1/324021996_868954454378806_9081909468718192652_n.jpg?stp=dst-jpg_p100x100&_nc_cat=105&ccb=1-7&_nc_sid=dbb9e7&_nc_ohc=j7Fwf14adzgAX9TR_Ix&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=00_AfBAE7jKeUnG1oNrTUYUgUTZxlKEla_EBysEL3rTj1DOiw&oe=63E8248F",
    //     },
    //   },
    // });
    navigate("/game");
  };

  return (
    <>
      {/* <SocketQuizferWrapper setRef={setRefSocket} /> */}
      <div className="container mx-auto">
        <If condition={isAuthenticated}>
          <Then>
            <div className="flex justify-center items-center relative">
              <img
                src={logged}
                alt=""
                className="w-full shadow-lg h-[150px] rounded-2xl"
              />
            </div>
            <div className="mt-5">
              <p className="  text-3xl font-bold mb-3">Recent</p>
              <div className="grid grid-cols-3 gap-8">
                {listCard.map((item, index) => (
                  <div key={index}>
                    <FlashCardSetInfo
                      id={1}
                      setName={item.set_name}
                      description={item.description}
                      tagName={item.tag_name}
                      numberOfTerms={item.flashcards.length}
                      author={item.author}
                    />
                  </div>
                ))}
              </div>
            </div>
            <div className="mt-5">
              <p className="  text-3xl font-bold mb-3">Recommend</p>
              <div className="grid grid-cols-3 gap-8">
                {listCard.map((item, index) => (
                  <div key={index}>
                    <FlashCardSetInfo
                      id={1}
                      setName={item.set_name}
                      numberOfTerms={item.flashcards.length}
                      author={item.author}
                    />
                  </div>
                ))}
              </div>
            </div>
          </Then>
          <Else>
            <Carousel autoplay className="mx-[20px] mt-[-20px] rounded-lg overflow-hidden">
              <div className="flex justify-center items-center relative">
                {/* <img src={banner} alt="" className="w-full shadow-lg h-[400px]"  /> */}
                <img
                  src={joinQuizFer}
                  alt=""
                  className="w-full shadow-lg h-[500px]"
                />
                <ButtonQuizfer
                  className="absolute bottom-[20%] right-20 w-[250px]"
                  color="purple"
                  onClick={() => setIsOpenEnterRoomCode(true)}
                >
                  Join Now
                </ButtonQuizfer>
              </div>
              <div>
                <img
                  src="https://i.pinimg.com/originals/5c/5d/66/5c5d6684644136c4b1442f1db30af6bf.gif"
                  alt=""
                  className="w-full shadow-lg h-[500px]"
                />
              </div>
            </Carousel>
            <div className="flex justify-center grid-cols-2 sm:grid-cols-4 gap-10 place-items-center pt-10">
              <div className="flex w-[35%] min-h-fit ">
                <img
                  src={ads1}
                  alt="ads2"
                  className="rounded-[20px] shadow-lg"
                />
              </div>
              <div className="block justify-end w-[40%] text-justify">
                <div>
                  <h2 className="text-3xl font-bold tracking-wide pb-5">
                    Memorise anything with free digital flashcards
                  </h2>
                </div>
                <div>
                  <p className="antialiased font-light">
                    Research shows that testing yourself with flashcards is more
                    effective than rereading your notes. Find existing
                    flashcards created by peers and teachers, or create your own
                    in a flash.
                  </p>
                </div>
              </div>
            </div>
            <div className="flex justify-center grid-cols-2 sm:grid-cols-4 gap-10 place-items-center pt-10">
              <div className="block justify-end w-[40%] text-justify">
                <div>
                  <h2 className="text-3xl font-bold tracking-wide pb-5">
                    From school to university, smash any subject
                  </h2>
                </div>
                <div>
                  <p className="antialiased font-light">
                    From maths to medicine to modern languages, QuizFer is used
                    by students in hundreds of different subjects at school,
                    university and beyond. Remember more, more efficiently — no
                    matter what youre studying.
                  </p>
                </div>
              </div>
              <div className="flex w-[35%] min-h-fit">
                <img
                  src={ads2}
                  alt="ads2 "
                  className="rounded-[20px] shadow-lg"
                />
              </div>
            </div>

            <section className="pt-10 services">
              <h1 className="heading space-x-4">
                <span>P</span>
                <span>R</span>
                <span>A</span>
                <span>C</span>
                <span>T</span>
                <span>I</span>
                <span>C</span>
                <span>E</span>
              </h1>
              <div className="box-container">
                <div className="box">
                  <CheckCircleOutlined className="text-3xl" />
                  <h3>Learn</h3>
                  <p>
                    When we improve our education and continue to learn, we can
                    foster new connections, increase our marketable skills, and
                    understand people better.
                  </p>
                </div>
                <div className="box">
                  <RiseOutlined className="text-3xl" />
                  <h3>Improve</h3>
                  <p>
                    Education helps us get exposure to new ideas and concepts
                    that we can use to appreciate and improve the world around
                    us and the world within us.
                  </p>
                </div>
                <div className="box">
                  <RedoOutlined className="text-3xl" />
                  <h3>Repeat</h3>
                  <p>
                    Learning allows us to make sense of the world around us, the
                    world inside of us, and where we fit within the world.
                    Education is the tool that breaks down all barriers. All
                    things are possible because anything can be learned.
                  </p>
                </div>
              </div>
            </section>
          </Else>
        </If>
      </div>

      <Modal
        open={isOpenEnterRoomCode}
        onCancel={() => {
          setRoomCode("");
          setIsOpenEnterRoomCode(false);
        }}
      >
        <div className="py-5 text-center">
          <p className="text-3xl font-bold mb-5  ">Room code</p>
          <InputQuizfer
            value={roomCode}
            className="mb-4"
            placeholder="Enter room code"
            onChange={setRoomCode}
          />
          <ButtonQuizfer color="blue" onClick={goToGame} disabled={!roomCode}>
            Join Now
          </ButtonQuizfer>
        </div>
      </Modal>
    </>
  );
};

export default HomePage;
