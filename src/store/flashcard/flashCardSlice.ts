import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { axiosInstance } from "config/axios";
import {
  ICreateFlashCardSetPayload,
  IFlashCardSetDetail,
  IFlashCardSetRespone,
  IStartLearnRespone,
  IUpdateFlashCardSetPayload,
} from "./types";
import { setLoading } from "store/global/globalSlice";

const initialState = {
  isUpdateFlashCardSet: false,
  isDeleteFlashCardSet: false,
};

export const getListFlashCardSet = createAsyncThunk(
  "flashcard/getListFlashCardSet",
  async (params: { type?: string }, { dispatch }) => {
    try {
      dispatch(setLoading(true));
      const data: IFlashCardSetRespone = await axiosInstance.get(
        "/api-gateway/flashcard-service/api/v1/flashcard/list",
        { params }
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const addMyNewFlashCardSet = createAsyncThunk(
  "flashcard/addMyNewFlashCardSet",
  async (payload: ICreateFlashCardSetPayload, { dispatch }) => {
    try {
      dispatch(setLoading(true));
      const data = await axiosInstance.post(
        "/api-gateway/flashcard-service/api/v1/flashcard",
        payload
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const editFlashCardSet = createAsyncThunk(
  "flashcard/editFlashCardSet",
  async (payload: IUpdateFlashCardSetPayload, { dispatch }) => {
    try {
      dispatch(setLoading(true));
      const data = await axiosInstance.post(
        "/api-gateway/flashcard-service/api/v1/flashcard/me/flashcard/updateV2",
        payload
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const deleteFlashCardSet = createAsyncThunk(
  "flashcard/deleteFlashCardSet",
  async (id: string, { dispatch }) => {
    try {
      dispatch(setLoading(true));
      const data = await axiosInstance.post(
        `/api-gateway/flashcard-service/api/v1/flashcard/me/flashcard/delete-set/${id}`
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const getDetailFlashCardSet = createAsyncThunk(
  "flashcard/getDetailFlashCardSet",
  async (id: string | number, { dispatch }) => {
    try {
      dispatch(setLoading(true));
      const data: IFlashCardSetDetail = await axiosInstance.get(
        `/api-gateway/flashcard-service/api/v1/flashcard/get-by-id/${id}`
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);
export const getListFolder = createAsyncThunk(
  "flashcard/getListFolder",
  // async (params: { type: string }, { dispatch }) => {
  async (_, { dispatch }) => {
    try {
      dispatch(setLoading(true));
      const data = await axiosInstance.get(
        `/api-gateway/flashcard-service/api/v1/folder/list-folder-flashcard-set`
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const startLearnFlashCard = createAsyncThunk(
  "flashcard/startLearnFlashCard",
  async (id: string, { dispatch }) => {
    try {
      dispatch(setLoading(true));
      const data: IStartLearnRespone = await axiosInstance.post(
        `/api-gateway/flashcard-service/api/v1/personalize/learn/start-session/${id}`
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const nextRoundLearn = createAsyncThunk(
  "flashcard/nextRoundLearn",
  async (
    body: { session_id: number; payload: { incorrect: []; correct: number[] } },
    { dispatch }
  ) => {
    try {
      dispatch(setLoading(true));
      const data: IStartLearnRespone = await axiosInstance.post(
        `/api-gateway/flashcard-service/api/v1/personalize/learn/next-round`,
        body.payload,
        { params: { session_id: body.session_id } }
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const getTestFlashCard = createAsyncThunk(
  "flashcard/getTestFlashCard",
  async (
    params: { flashCardSetId: number; numberQuestion: number | null},
    { dispatch }
  ) => {
    try {
      dispatch(setLoading(true));
      const data = await axiosInstance.get(
        `/api-gateway/flashcard-service/api/v1/test/list/question/test`,
        { params }
      );
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const flashCardSlice = createSlice({
  name: "flashcard",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addMyNewFlashCardSet.pending, (state) => {})
      .addCase(addMyNewFlashCardSet.fulfilled, (state) => {})
      .addCase(getListFlashCardSet.pending, (state) => {})
      .addCase(getDetailFlashCardSet.pending, (state) => {
        state.isUpdateFlashCardSet = false;
        state.isDeleteFlashCardSet = false;
      })
      .addCase(editFlashCardSet.fulfilled, (state) => {
        state.isUpdateFlashCardSet = true;
      })
      .addCase(deleteFlashCardSet.fulfilled, (state) => {
        state.isDeleteFlashCardSet = true;
      });
  },
});

export const {} = flashCardSlice.actions;

export default flashCardSlice.reducer;
