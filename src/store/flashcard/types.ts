export interface IFlashCardPayload {
  term: string;
  definitions: string[];
}

export interface IFlashCardUpdatePayload {
  term: string;
  definitions: {
    definition: string;
  }[];
}

export interface ICreateFlashCardSetPayload {
  flash_card_set_id?: number | string;
  set_name?: string;
  description?: string;
  flashcards: IFlashCardPayload[];
  tag_name?: string[];
}

export interface IUpdateFlashCardSetPayload {
  flash_card_set_id?: number | string;
  set_name?: string;
  description?: string;
  flashcards: IFlashCardUpdatePayload[];
  tag_name?: string[];
}

export interface IMetaData {
  viewer: number;
  rate: number;
  flash_card_id: number;
}

export interface IDefinition {
  id: number;
  flashcard_id?: number;
  definition: string;
  is_correct: boolean;
  definition_order?: number;
}

export interface IFlashCard {
  id?: number;
  set_id?: number;
  term: string;
  No?: number;
  definitions: IDefinition[];
}

export interface IFlashCardSetRespone {
  data: IFlashCardSet[];
  currentPage: number;
  total_items: number;
  total_pages: number;
}
export interface IAuthor {
  userId: number;
  firstName: string;
  lastName: string;
  email: string;
  name: string;
}
export interface IFlashCardSet {
  author: IAuthor;
  author_id: number;
  encrypted: boolean;
  set_name: string;
  set_id: number | string;
  description: string;
  meta_data: IMetaData;
  tag_name: string[];
  total_terms: number;
}
export interface IFlashCardSetDetail {
  author?: IAuthor;
  set_id: number;
  author_id: number;
  encrypted: boolean;
  set_name: string;
  description: string;
  meta_data: IMetaData;
  flashcards: IFlashCard[];
  tag_name: string[];
}
export interface ILearnSession {
  session_id: number;
  user_id: number;
  flashcard_set_id: number;
  term_per_round: number;
  current_round: number;
  status: string;
  created_at: Date;
  // rounds: Round[];
}
export interface CurrentRound {
  learn_id: number;
  answer_times: number;
  is_correct: boolean;
  created_at: Date;
  flashcard_data: IFlashCard;
  correct_answer: number;
  incorrect_answer?: any;
  is_answered: boolean;
  retry?: boolean;
}

export interface IStartLearnRespone {
  learn_session: ILearnSession;
  current_round: CurrentRound[];
}
