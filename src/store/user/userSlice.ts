import { setLoading } from "store/global/globalSlice";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { axiosInstance } from "config/axios";
import { IUserInfo } from "./types";

interface IState {
  userInfo: IUserInfo | null;
}

const initialState: IState = {
  userInfo: {
    email: "",
    user_name: "",
    first_name: "",
    last_name: "",
    full_name: "",
    date_of_birth: "",
  },
};

export const getProfile = createAsyncThunk(
  "auth/getProfile",
  async (_, { dispatch }) => {
    try {
      dispatch(setLoading(true));
      const data = await axiosInstance.post(
        "/api-gateway/user-service-api/api/v1/profile/info"
      );
      dispatch(setUserInfo(data));
      dispatch(setLoading(false));
      return data;
    } catch (err) {
      dispatch(setLoading(false));
      return false;
    }
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserInfo(state, { payload }) {
      state.userInfo = payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getProfile.pending, (state) => {});
    builder.addCase(getProfile.fulfilled, (state) => {});
  },
});

export const { setUserInfo } = userSlice.actions;

export default userSlice.reducer;
