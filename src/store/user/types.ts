export interface IUserInfo {
  email: string;
  user_name: string;
  first_name: string;
  last_name: string;
  full_name: string;
  date_of_birth: Date | string;
}
