import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { axiosInstance } from "config/axios";
import { IFlashCardSet } from "store/flashcard/types";

interface IState {
  loading: boolean;
}

const initialState: IState = {
  loading: false,
};

export const searchBy = createAsyncThunk(
  "global/searchBy",
  async (params: {
    by: string;
    "search-value": string | null;
    orderBy: string;
  }) => {
    try {
      return await axiosInstance.get(
        "/api-gateway/flashcard-service/api/v1/flashcard/global/search-flashcard",
        { params }
      );
    } catch (err) {
      return false;
    }
  }
);

export const globalSlice = createSlice({
  name: "global",
  initialState,
  reducers: {
    setLoading(state, { payload }) {
      state.loading = payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(searchBy.pending, (state) => {
      state.loading = true
    }).addCase(searchBy.fulfilled, (state) => {
      state.loading = false
    })
  },
});

export const { setLoading } = globalSlice.actions;

export default globalSlice.reducer;
