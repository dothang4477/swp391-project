// import thunkMiddleware from "redux-thunk";
import globalSlice from "./global/globalSlice";
import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./auth/authSlice";
import userSlice from "./user/userSlice";
import flashCardSlice from "./flashcard/flashCardSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    user: userSlice,
    flashCard: flashCardSlice,
    global: globalSlice,
  },
  // middleware: [thunkMiddleware],
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
