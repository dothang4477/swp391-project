import { AuthContext } from "config/context/auth";
import { FC, memo, useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
interface IProtectedRouteProps {
  children?: FC | any;
}
const ProtectedRoute: FC<IProtectedRouteProps> = ({ children }) => {
  const navigate = useNavigate();
  const { isAuthenticated } = useContext(AuthContext);
  useEffect(() => {
    window.scrollTo(0, 0);
    if (!isAuthenticated) {
      navigate("/");
    }
  }, [isAuthenticated]);

  return <>{children}</>;
};

export default memo(ProtectedRoute);
