import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import DefaultLayout from "layout/default";
import HomePage from "pages/home";
import ProtectedRoute from "./components/ProtectedRoute";
import GamePage from "pages/game";
import UserProfile from "pages/profile";
import CreateFlashCardSetPage from "pages/flashcard-set/create";
import CollectionsPage from "pages/collections";
import FlashCardSetPage from "pages/flashcard-set";
import TestFlashCardSetPage from "pages/flashcard-set/test";
import FlashCardSetDetailPage from "pages/flashcard-set/detail";
import ServerErrorPage from "pages/server-error";
import LearnFlashCardSetPage from "pages/flashcard-set/learn/index";
import { SearchPage } from "../pages/search/index";
import { Spin } from "antd";
import { useAppSelector } from "reduxHook";
import ClassPage from "pages/class";
import FolderPage from "../pages/folder/index";

const Routing = () => {
  const loading = useAppSelector((state) => state.global.loading);
  return (
    <Router>
      <Spin spinning={loading} tip="Loading..." size="large">
        <Routes>
          <Route path="/" element={<DefaultLayout />}>
            <Route index element={<HomePage />} />
            <Route
              path="game"
              element={<ProtectedRoute children={<GamePage />} />}
            />
            <Route
              path=":name"
              element={<ProtectedRoute children={<UserProfile />} />}
            />
            <Route
              path="collections"
              element={<ProtectedRoute children={<CollectionsPage />} />}
            />
            <Route
              path=":username/folders/:foldername/sets"
              element={<ProtectedRoute children={<FolderPage />} />}
            />
            <Route
              path="flashcard-set"
              element={<ProtectedRoute children={<FlashCardSetPage />} />}
            >
              <Route
                path="create"
                element={
                  <ProtectedRoute children={<CreateFlashCardSetPage />} />
                }
              />
              <Route
                path=":id"
                element={
                  <ProtectedRoute children={<FlashCardSetDetailPage />} />
                }
              />
            </Route>
            <Route
              path="search"
              element={<ProtectedRoute children={<SearchPage />} />}
            />
            <Route
              path="class/:id"
              element={<ProtectedRoute children={<ClassPage />} />}
            />
            <Route
              path="server-error"
              element={<ProtectedRoute children={<ServerErrorPage />} />}
            />
          </Route>
          <Route path="/flashcard-set">
            <Route
              path=":id/learn"
              element={<ProtectedRoute children={<LearnFlashCardSetPage />} />}
            />
            <Route
              path=":id/test"
              element={<ProtectedRoute children={<TestFlashCardSetPage />} />}
            />
          </Route>
          <Route path="*" element={<>404 Error</>} />
        </Routes>
      </Spin>
    </Router>
  );
};

export default Routing;
