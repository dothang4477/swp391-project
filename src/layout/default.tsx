import React from "react";
import { Outlet } from "react-router-dom";
import Header from "components/header";
import Footer from "components/footer";

function DefaultLayout() {
  return (
    <>
      <Header />
      <div className="py-[40px] px-5 sm:px-[10%] min-h-[100vh] mx-auto bg-rose-light">
        <Outlet />
      </div>
      <Footer />
    </>
  );
}

export default DefaultLayout;
