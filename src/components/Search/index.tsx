import { useEffect, useMemo, useState } from "react";
import { Col, Divider, Dropdown, Row, Select, Space } from "antd";
import FlashCardSetInfo from "components/FlashCardSet/CardInfo";
import { useLocation } from "react-router-dom";
import { useAppDispatch } from "reduxHook";
import { searchBy } from "store/global/globalSlice";
import { IFlashCardSet } from "store/flashcard/types";
import SwiperQuizfer from "components/ui/Swiper";

const Search = () => {
  const { search } = useLocation();
  const dispatch = useAppDispatch();
  const query = new URLSearchParams(search);
  const [keyword, setKeyword] = useState<string | null>(query.get("keyword"));
  const [filterValue, setfilterValue] = useState("author");
  const [listSearch, setListSearch] = useState<IFlashCardSet[]>([]);

  useEffect(() => {
    fetchDataSearch();
  }, [keyword, filterValue]);

  useEffect(() => {
    setKeyword(query.get("keyword"));
  }, [query.get("keyword")]);

  const fetchDataSearch = async () => {
    const { payload: data } = await dispatch(
      searchBy({
        by: filterValue,
        "search-value": keyword,
        orderBy: "",
      })
    );
    if (data) {
      setListSearch(data as IFlashCardSet[]);
    }
  };

  const renderListSearch = useMemo(() => {
    let countItem = 0;
    const cookList: IFlashCardSet[][] = [];
    let group: IFlashCardSet[] = [];
    listSearch.forEach((item, index) => {
      if (countItem < 9) {
        group.push(item);
        countItem++;
      } else {
        cookList.push(group);
        group = [];
        countItem = 0;
      }
      if (index === listSearch.length - 1) {
        cookList.push(group);
      }
    });
    return cookList.map((item) => {
      return (
        <div className="grid grid-cols-3 gap-10 justify-center">
          {item.map((item_cook, index) => (
            <div key={index}>
              <FlashCardSetInfo
                id={item_cook.set_id}
                setName={item_cook.set_name}
                description={item_cook.description}
                tagName={item_cook.tag_name}
                numberOfTerms={item_cook.total_terms}
                author={item_cook.author}
              />
            </div>
          ))}
        </div>
      );
    });
  }, [listSearch]);

  return (
    <>
      <div className="text-center">
        <h1 className="text-4xl font-bold">Search page</h1>
        <Divider className="bg-slate-500" />
        <Row className="justify-between items-center mb-8">
          <Col className="text-3xl font-bold">
            <p>
              Found {listSearch.length} result(s) for "{keyword}"
            </p>
          </Col>
          <Col className="flex items-center gap-3">
            <p className="text-xl font-bold">Filter by:</p>
            <Select
              className="min-w-[200px]"
              defaultValue={filterValue}
              onChange={setfilterValue}
              options={[
                { value: "author", label: "Author" },
                { value: "class", label: "Class" },
                { value: "name", label: "Flashcard Set" },
              ]}
            />
          </Col>
        </Row>
      </div>
      <SwiperQuizfer
        slides={renderListSearch}
        options={{
          slidesPerView: 1,
          spaceBetween: 40,
        }}
      />
      <div className="flex justify-center">
        <div className="grid grid-cols-4 gap-6"></div>
      </div>
    </>
  );
};

export default Search;
