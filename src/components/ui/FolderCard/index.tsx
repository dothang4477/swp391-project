import { Card, Row } from "antd";
import { FC } from "react";
import { useNavigate } from "react-router";
import { FolderOpenOutlined } from "@ant-design/icons";
interface IFolderCardInfoProps {
  folderInfo: any;
}
const FolderCardInfo: FC<IFolderCardInfoProps> = ({ folderInfo }) => {
  const navigate = useNavigate();
  return (
    <Card
      role="button"
      className="bg-white w-full p-2 shadow-md rounded-xl border-b-4 border-b-amber-500 hover:scale-105"
    >
      <Row className="ml-5">
        <div className="flex gap-5">
          <FolderOpenOutlined className="text-3xl" />
          <p className="font-bold text-2xl">
            {folderInfo?.folderName || "HieuDepTrai"}
          </p>
        </div>
      </Row>
      <Row className="w-full">
        <div className="flex gap-5 m-3">
          <img
            width={40}
            src={
              folderInfo?.author?.avatar ||
              "https://i.pinimg.com/474x/44/bc/1a/44bc1adefc096b4115d9a016b4d0e062.jpg"
            }
            alt=""
            className="rounded-full"
          />
          <p className="font-medium text-lg">
            {" "}
            | {folderInfo?.flashcards?.total || "(423) terms"} terms
          </p>
        </div>
      </Row>
    </Card>
  );
};

export default FolderCardInfo;
