import { DeleteOutlined } from "@ant-design/icons";
import { Modal, ModalFuncProps } from "antd";
import { FC } from "react";
import ButtonQuizfer from "../button";
import "./style.scss"

const DeleteModal: FC<ModalFuncProps> = (props) => {
  return (
    <Modal className="text-center delete-modal" title={props?.title || "Delete Comfirm"} open={props.open} onCancel={props.onCancel}>
     <div className="py-3">
     <p className="text-base mb-10">Do you want to delete? This action can't undo!</p>
     <div className="flex gap-6 justify-center">
     <ButtonQuizfer color="red" onClick={props.onOk}>
     <DeleteOutlined className="mr-2" />
      Yes Delete</ButtonQuizfer>
      <ButtonQuizfer className="text-gray-dark" onClick={props.onCancel}>Cancel</ButtonQuizfer>
     </div>
     </div>
    </Modal>
  );
};
export default DeleteModal;
