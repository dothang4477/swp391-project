import React, { useMemo, useRef, useState } from "react";
import { Pagination } from "antd";
import { Swiper, SwiperSlide } from "swiper/react";
import { SwiperOptions } from "swiper/types";
import { If, Then } from "react-if";

interface SwiperProps {
  slides: any[];
  options?: SwiperOptions;
}

const SwiperQuizfer: React.FC<SwiperProps> = ({ slides, options }) => {
  const swiperRef = useRef<any>(null);
  const [currentPage, setCurrentPage] = useState(1);

  const itemPerPage = useMemo(
    () => (options?.slidesPerView as number) || 1,
    [options]
  );

  const formatList = useMemo(() => {
    if (slides.length % itemPerPage > 0) {
      for (const i = 0; i < slides.length % itemPerPage; ) {
        slides.push(<></>);
      }
    }
    return slides;
  }, [slides, itemPerPage]);

  const handleSlideChange = ({ activeIndex }: any) => {
    setCurrentPage(Math.floor(activeIndex / itemPerPage) + 1);
  };

  const handleChangePage = (page: number) => {
    swiperRef.current.swiper.slideTo((page - 1) * itemPerPage);
  };

  return (
    <div className="relative">
      <Swiper
        className="mb-3"
        {...options}
        onSlideChange={handleSlideChange}
        ref={swiperRef}
      >
        {formatList.map((slide, index) => (
          <SwiperSlide className="p-4" key={`slide-${index}`}>{slide}</SwiperSlide>
        ))}
      </Swiper>
      <If condition={formatList.length > itemPerPage}>
        <Then>
          <Pagination
            className="text-center font-bold"
            current={currentPage}
            pageSize={itemPerPage}
            total={formatList.length}
            showQuickJumper={formatList.length / itemPerPage > 10}
            onChange={(page, _) => handleChangePage(page)}
          />
        </Then>
      </If>
    </div>
  );
};

export default SwiperQuizfer;
