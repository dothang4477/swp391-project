import { Card, Row, Col, Popover, Button } from "antd";
import { FC } from "react";
import { useNavigate } from "react-router";
import { IdcardOutlined, DeleteOutlined } from "@ant-design/icons";
interface IMemberCardInfoProps {
  memberInfo: any;
}
const MemberCardInfo: FC<IMemberCardInfoProps> = ({ memberInfo }) => {
  const navigate = useNavigate();
  return (
    <Card
      role="button"
      className="bg-white w-auto py-3 px-10 min-h-[100px] shadow-md border-b-2 border-b-green hover:scale-105"
    >
      <div className="flex w-full justify-between gap-10">
        <div className="flex gap-5">
          <img
            width={60}
            src={
              memberInfo?.avatar ||
              "https://i.pinimg.com/474x/44/bc/1a/44bc1adefc096b4115d9a016b4d0e062.jpg"
            }
            alt=""
            className="h-[60px] rounded-full "
          />
          <div className="flex flex-col gap-3">
            <p className="font-bold text-2xl">
              {memberInfo?.name || "HieuBeo"}
            </p>
            <p className="flex font-semibold text-xl text-zinc-500">
              <IdcardOutlined className="mr-4" />
              <p>{memberInfo?.role || "Teacher"}</p>
            </p>
          </div>
        </div>
        <div>
          <Popover
            placement="bottomRight"
            content={
              <Button className="text-red-500">
                <DeleteOutlined /> Remove from class
              </Button>
            }
            trigger="click"
          >
            <Button>...</Button>
          </Popover>
        </div>
      </div>
    </Card>
  );
};

export default MemberCardInfo;
