import { Card, Row, Divider } from "antd";
import { FC } from "react";
import { useNavigate } from "react-router";
import { IdcardOutlined } from "@ant-design/icons";
interface IClassCardInfoProps {
  classInfo: any;
}
const ClassCardInfo: FC<IClassCardInfoProps> = ({ classInfo }) => {
  const navigate = useNavigate();
  return (
    <Card
      role="button"
      className="bg-white w-full p-4 shadow-md rounded-xl border-b-4 border-b-pink hover:scale-105"
    >
      <Row className="w-full">
        <div className="flex gap-5">
          <IdcardOutlined className="font-bold text-3xl" />
          <p className="font-bold text-2xl">
            | {classInfo?.name || "(423) terms"}
          </p>
        </div>
      </Row>
      <Row className="ml-5 mt-3">
        <div className="flex gap-5">
          <p className="font-medium text-xl">
            {classInfo?.totalSet || "12"} sets
          </p>
          <Divider type="vertical" className="border-indigo-500 h-auto" />
          <p className="font-medium text-xl">
            {classInfo?.totalMember || "4"} members
          </p>
          <Divider type="vertical" className="border-indigo-500 h-auto" />
          <p className="font-light text-xl">
            Located: {classInfo?.location || "FPT University"}
          </p>
        </div>
      </Row>
    </Card>
  );
};

export default ClassCardInfo;
