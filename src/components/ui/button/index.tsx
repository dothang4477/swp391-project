import React, { FC } from "react";
import { useMemo } from "react";
interface IButtonQuizferProps {
  className?: string | any;
  disabled?: boolean;
  children?: string | any;
  color?: "blue" | "yellow" | "red" | "purple" | "green" | string | any;
  onClick?: () => void;
}
const ButtonQuizfer: FC<IButtonQuizferProps> = ({
  children,
  className = "",
  color = "",
  disabled,
  onClick,
}) => {
  const styleBtn = useMemo(() => {
    let initStyleBtn = "";
    switch (color) {
      case "blue":
        initStyleBtn = "bg-blue text-white hover:bg-white hover:text-blue";
        break;
      case "yellow":
        initStyleBtn = "bg-yellow text-purple hover:bg-white hover:text-yellow";
        break;
      case "red":
        initStyleBtn =
          "bg-red-600 text-white hover:bg-white hover:text-red-600";
        break;
      case "purple":
        initStyleBtn = "bg-purple text-white hover:bg-white hover:text-purple";
        break;
      case "green":
        initStyleBtn =
          "bg-green text-white hover:bg-white hover:border-green hover:text-green";
        break;
      default:
        initStyleBtn = "text-purple bg-white hover:bg-slate-100";
        break;
    }
    return `shadow-md flex justify-around items-center items px-5 py-3 rounded-lg font-bold text-base text-center ${initStyleBtn} ${className} ${
      disabled && "pointer-events-none"
    }`;
  }, [className, color, disabled]);

  return (
    <div
      role="button"
      style={
        disabled
          ? {
              background: "#e3e8f0",
            }
          : {}
      }
      className={styleBtn}
      onClick={onClick}
    >
      <div className="w-full leading-none whitespace-nowrap">{children}</div>
    </div>
  );
};

export default ButtonQuizfer;
