import { Input, InputProps } from "antd";
import { FC } from "react";
interface IInputQuizferProps {
  className?: string;
  label?: string;
  disabled?: boolean;
  type?: string;
  required?: boolean;
  placeholder?: string;
  value?: string;
  error?: string | boolean | any;
  onEnter?: () => void;
  onChange?: (value: string) => void;
}
const InputQuizfer: FC<IInputQuizferProps> = ({
  label,
  disabled = false,
  value,
  required = false,
  type = "text",
  placeholder,
  className,
  error,
  onEnter,
  onChange,
}) => {
  return (
    <div className={className}>
      {label && (
        <p className="font-bold mb-2 text-lg">
          {label} {required && <span className="text-red-600">*</span>}
        </p>
      )}
      <Input
        disabled={disabled}
        value={value}
        type={type}
        size="large"
        className={`bg-white border-b-2 border-b-purple ${error && "border-b-red-600"}`}
        placeholder={placeholder || `Enter ${label}`}
        onChange={(e) => onChange && onChange(e.target.value)}
        onKeyUp={(e) => {
          if (e.code === "Enter" && value) {
            onEnter && onEnter();
          }
        }}
      />
      {error && <p className="text-sm text-red-500 mt-1">{error}</p>}
    </div>
  );
};

export default InputQuizfer;
