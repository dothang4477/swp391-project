import { FC, useState } from "react";
import { useSpring, animated } from "react-spring";
import "./style.scss";
import { FlagOutlined } from "@ant-design/icons";
interface IFlipCardProps {
  term: string | any;
  definition: string;
  indexCard?: string;
}
const FlipCard: FC<IFlipCardProps> = ({ term, definition, indexCard }) => {
  const [flipped, set] = useState(false);
  const { transform, opacity } = useSpring({
    opacity: flipped ? 1 : 0,
    transform: `perspective(600px) rotateX(${flipped ? 180 : 0}deg)`,
    config: { mass: 5, tension: 500, friction: 80 },
  });
  return (
    <div
      className="card text-xl font-bold"
      onClick={() => set((state) => !state)}
    >
      <animated.div
        role="button"
        className="cardSide cardSideFront rounded-xl"
        style={{ opacity: opacity.interpolate((o) => 1 - o), transform }}
      >
        <div className="flex justify-between absolute top-3 left-5 right-5">
          <h1>Term</h1>
          <p>{indexCard}</p>
          <FlagOutlined className="text-2xl" />
        </div>

        <h1 className="break-all">{term}</h1>
      </animated.div>
      <animated.div
        role="button"
        className="cardSide cardSideBack rounded-xl"
        style={{
          opacity,
          transform: transform.interpolate((t) => `${t} rotateX(180deg)`),
        }}
      >
        <div className="flex justify-between absolute top-3 left-5 right-5">
          <h1>Definition</h1>
          <FlagOutlined className="text-2xl" />
        </div>
        <h1 className="break-all">{definition}</h1>
      </animated.div>
    </div>
  );
};

export default FlipCard;
