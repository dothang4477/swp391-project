import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { FC, useMemo } from "react";

interface IAnswerButtonProps {
  answerText: string;
  status?: string | "success" | "failed" | "selected";
  disabled?: boolean;
  onClick?: (value: string | any) => void;
}

const AnswerButton: FC<IAnswerButtonProps> = ({
  answerText,
  status,
  disabled,
  onClick,
}) => {
  const renderStatusIcon = useMemo(() => {
    switch (status) {
      case "success":
        return <CheckOutlined className="text-green text-xl" />;
      case "failed":
        return <CloseOutlined className="text-red-600 text-xl" />;
      default:
        return <></>;
    }
  }, [status]);

  const styleAnswerButton = useMemo(() => {
    let initStyle = `flex justify-between items-center text-lg font-bold border-2 px-5 py-3 rounded-lg `;
    switch (status) {
      case "success":
        initStyle +=
          "bg-green-light text-green border-green pointer-events-none";
        break;
      case "failed":
        initStyle +=
          "bg-red-50 text-red-600 border-red-400 pointer-events-none";
        break;
      case "selected":
        initStyle += "bg-sky-50 text-blue border-blue pointer-events-none";
        break;
      default:
        initStyle += `border-gray-light hover:text-blue hover:border-blue cursor-pointer ${
          disabled && "opacity-40 pointer-events-none"
        }`;
    }
    return initStyle;
  }, [status, disabled]);

  return (
    <div
      className={styleAnswerButton}
      onClick={() => onClick && onClick(answerText)}
    >
      <p>{answerText}</p>
      <div>{renderStatusIcon}</div>
    </div>
  );
};
export default AnswerButton;
