import { Divider, Tabs, Tag } from "antd";
import { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Carousel } from "antd";
import {
  FolderOutlined,
  IdcardOutlined,
  SnippetsOutlined,
} from "@ant-design/icons";
import Folders from "./Folders";
import Sets from "./Sets";
import Classes from "./Classes";
const banner1 = require("images/collections_banner1.png");
const banner2 = require("images/collections_banner2.png");
const banner3 = require("images/collections_banner3.png");

export const menuProfile = [
  {
    key: "sets",
    tile: "Flashcard Sets",
    icon: <SnippetsOutlined className="text-2xl" />,
    children: <Sets />,
  },
  {
    key: "folders",
    tile: "Folders ",
    icon: <FolderOutlined className="text-2xl" />,
    children: <Folders />,
  },
  {
    key: "classes",
    tile: "Classes",
    icon: <IdcardOutlined className="text-2xl" />,
    children: <Classes />,
  },
];

const CollectionList = () => {
  const navigate = useNavigate();
  let { search } = useLocation();
  const query = new URLSearchParams(search);
  const [currentTab, setCurrentTab] = useState(query.get("tab") || "sets");

  useEffect(() => {
    navigate(`?tab=${currentTab}`);
  }, []);

  return (
    <>
      <div className="flex w-full justify-between">
        <div className="flex gap-5">
          <img
            width={80}
            src={
              "https://i.pinimg.com/474x/44/bc/1a/44bc1adefc096b4115d9a016b4d0e062.jpg"
            }
            alt=""
            className="h-[80px] rounded-full"
          />
          <div>
            <p className="font-bold text-2xl mb-2">Nguyen Huy Hieu</p>
            <Tag className="text-base">Teacher</Tag>
          </div>
        </div>
        <div className="relative w-2/3">
          <Carousel autoplay className="rounded-lg overflow-hidden">
            <img src={banner2} className="h-[150px]" alt="" />
            <img src={banner1} className="h-[150px]" alt="" />
            <img src={banner3} className="h-[150px]" alt="" />
          </Carousel>
        </div>
      </div>
      <Divider className="shadow-md border-purple" />
      <Tabs
        defaultActiveKey={currentTab}
        size="middle"
        onChange={(tab) => {
          setCurrentTab(tab);
          navigate(`?tab=${tab}`);
        }}
        items={menuProfile.map((item) => {
          return {
            label: (
              <div className="font-bold">
                {item.icon}
                <span className="mb-0 text-lg">{item.tile}</span>
              </div>
            ),
            key: item.key,
            children: item.children,
          };
        })}
      />
    </>
  );
};

export default CollectionList;
