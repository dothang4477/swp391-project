import { FolderAddOutlined } from "@ant-design/icons";
import { Modal } from "antd";
import { useState } from "react";
import ButtonQuizfer from "components/ui/button";
import InputQuizfer from "components/ui/input";
import FolderCardInfo from "components/ui/FolderCard";

export const Folders = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <div className="flex justify-between my-5">
        <p className="text-3xl font-bold">My Folders</p>
        <ButtonQuizfer
          className="border bg-purple text-slate-50 hover:text-purple"
          onClick={showModal}
        >
          <FolderAddOutlined className="text-xl mr-3" />
          Create new folder
        </ButtonQuizfer>
      </div>
      <FolderCardInfo
        folderInfo={{
          folderName: "HieudepTrai",
          author: { avatar: "" },
          flashcards: { total: 423 },
        }}
      />
      <Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <p className="text-3xl font-bold mb-5">
          <FolderAddOutlined /> Create New Folder
        </p>
        <InputQuizfer
          value={""}
          className="mb-5"
          label="Folder Name"
          required
        />
        <InputQuizfer value={""} label="Description" />
        <ButtonQuizfer className="mt-5 border-none" color="purple">
          Create
        </ButtonQuizfer>
      </Modal>
    </>
  );
};

export default Folders;
