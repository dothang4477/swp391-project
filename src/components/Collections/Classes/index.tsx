import { ClusterOutlined } from "@ant-design/icons";
import { Modal } from "antd";
import { useState } from "react";
import ButtonQuizfer from "components/ui/button";
import InputQuizfer from "components/ui/input";
import ClassCardInfo from "components/ui/ClassCard";

export const Classes = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <div className="flex justify-between my-5">
        <p className="text-3xl font-bold">My Classes</p>
        <ButtonQuizfer
          className="border bg-purple text-slate-50 hover:text-purple"
          onClick={showModal}
        >
          <ClusterOutlined className="text-xl mr-3" />
          Create new class
        </ButtonQuizfer>
      </div>
      <ClassCardInfo
        classInfo={{
          totalSet: 123,
          totalMember: 23,
          location: "FPT University HL",
          name: "HieuDepTrai",
        }}
      />
      <Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <p className="text-3xl font-bold mb-5">
          <ClusterOutlined /> Create New Class
        </p>
        <InputQuizfer value={""} className="mb-5" label="Class Name" required />
        <InputQuizfer value={""} label="Description" />
        <InputQuizfer value={""} label="School" />
        <ButtonQuizfer className="mt-5 border-none" color="purple">
          Create
        </ButtonQuizfer>
      </Modal>
    </>
  );
};

export default Classes;
