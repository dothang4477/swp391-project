import { DiffFilled } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import ButtonQuizfer from "components/ui/button";
import { useEffect, useState } from "react";
import { useAppDispatch } from "reduxHook";
import FlashCardSetInfo from "components/FlashCardSet/CardInfo";
import { getListFlashCardSet } from "store/flashcard/flashCardSlice";
import SwiperQuizfer from "components/ui/Swiper";
import { IFlashCardSet } from "store/flashcard/types";

export const Sets = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [listMyFlashCardSet, setListMyFlashCardSet] = useState<IFlashCardSet[]>(
    []
  );

  useEffect(() => {
    dispatch(getListFlashCardSet({ type: "" })).then(
      ({ payload: { data } }: any) => {
        if (data) {
          setListMyFlashCardSet(data);
        }
      }
    );
  }, []);

  return (
    <>
      <div className="flex justify-between my-5">
        <p className="text-3xl font-bold">My FlashCards</p>
        <ButtonQuizfer
          className="border bg-purple text-slate-50 hover:text-purple"
          onClick={() => navigate("/flashcard-set/create")}
        >
          <DiffFilled className="text-xl mr-3" />
          Create new FlashCard set
        </ButtonQuizfer>
      </div>
      <div className="mb-10">
        <SwiperQuizfer
          slides={listMyFlashCardSet.map((item, index) => (
            <FlashCardSetInfo
              key={index}
              id={item.set_id}
              setName={item.set_name}
              description={item.description}
              tagName={item.tag_name}
              numberOfTerms={item.total_terms}
              author={item.author}
            />
          ))}
          options={{
            slidesPerView: 4,
            spaceBetween: 10,
          }}
        />
      </div>
    </>
  );
};

export default Sets;
