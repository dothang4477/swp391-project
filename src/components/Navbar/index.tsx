import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Search from "antd/es/input/Search";
import AuthNavbar from "./Auth";
import MenuNavbar from "./MenuNavbar";

const NavBar: React.FC<any> = () => {
  const navigate = useNavigate();
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  const [searchValue, setSearchValue] = useState(query.get("keyword") || "");
  return (
    <div className="bg-header-banner min-w-screen sticky top-0 px-3 shadow-md z-50">
      <div className="px-32 flex w-full items-center">
        <div className="flex gap-20 items-center">
          <p
            role="button"
            className="text-white text-3xl font-bold"
            onClick={() => navigate("/")}
          >
            QuizFer
          </p>
          <MenuNavbar />
        </div>
        <div className="ml-10 flex justify-end gap-10 items-center w-full">
          <Search
            value={searchValue}
            className="max-w-[400px] h-8 rounded-lg focus-within:scale-105 focus-within:shadow-md"
            placeholder="What you wanna searching..."
            allowClear
            onChange={(e) => setSearchValue(e.target.value)}
            onSearch={() => {
              navigate(`/search?keyword=${searchValue}`);
            }}
          />
          <AuthNavbar />
        </div>
      </div>
    </div>
  );
};

export default NavBar;
