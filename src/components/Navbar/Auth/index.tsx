import {
  BellFilled,
  CloseOutlined,
  PoweroffOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Avatar, Drawer, Dropdown, MenuProps } from "antd";
import AuthTemplate from "components/Auth/AuthTemplate";
import { useAuthFormik } from "components/Auth/AuthTemplate/hooks";
import ButtonQuizfer from "components/ui/button";
import { AuthContext } from "config/context/auth";
import { FormikContext } from "formik";
import React, { useContext, useEffect, useState } from "react";
import { Else, If, Then } from "react-if";
import { useNavigate } from "react-router-dom";
import { useAppSelector } from "reduxHook";
const loginBanner = require("images/login.png");

function AuthNavbar() {
  const navigate = useNavigate();
  const { isAuthenticated } = useContext(AuthContext);
  const { formik, handleSubmit } = useAuthFormik();
  const [openAuth, setOpenAuth] = useState(false);
  const [typeAuth, setTypeAuth] = useState("signin");
  const userInfo = useAppSelector((state) => state.user.userInfo);

  const ItemSetting: MenuProps["items"] = [
    {
      label: (
        <div className="flex items-center gap-2">
          <Avatar
            className="w-10 h-10 cursor-pointer"
            src="https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80"
          />
          <div>
            <p className=" text-gray-dark font-bold mb-0">
              {userInfo?.full_name || "..."}
            </p>
            <p className=" text-slate-500 text-sm">
              {userInfo?.email || "..."}
            </p>
          </div>
        </div>
      ),
      type: "group",
    },
    {
      type: "divider",
    },
    {
      label: (
        <div onClick={() => navigate(`/${userInfo?.user_name}?tab=overview`)}>
          <UserOutlined className="mr-2" />
          Management
        </div>
      ),
      key: "0",
    },
    {
      type: "divider",
    },
    {
      label: (
        <div
          className="text-red-600"
          onClick={() => {
            localStorage.removeItem("token");
            window.location.reload();
          }}
        >
          <PoweroffOutlined className="mr-2" />
          Logout
        </div>
      ),
      key: "3",
    },
  ];

  useEffect(() => {
    formik.resetForm();
    formik.setFieldValue("typeSubmit", typeAuth);
    if (typeAuth === "signin") {
      formik.setFieldValue("first_name", "none");
      formik.setFieldValue("last_name", "none");
      formik.setFieldValue("email", "none@none");
      formik.setFieldValue("date_of_birth", "none");
    }
  }, [typeAuth]);

  const renderAuthTemplate = (
    <Drawer
      className="p-0"
      closable={false}
      placement={"top"}
      height="100%"
      open={openAuth}
    >
      <div className="flex h-full">
        <img className="w-1/2" src={loginBanner} alt="" />
        <div className="w-1/2 px-[5%] py-10 overflow-y-scroll">
          {/* <img className="mx-auto mb-5" src={logo} width={200} alt="" /> */}
          <p className="text-purple text-5xl font-bold text-center mb-5">
            QuizFer
          </p>
          <div className="flex justify-center gap-5">
            <ButtonQuizfer
              color={typeAuth === "signin" && "yellow"}
              onClick={() => setTypeAuth("signin")}
            >
              Signin
            </ButtonQuizfer>
            <ButtonQuizfer
              color={typeAuth === "signup" && "yellow"}
              onClick={() => setTypeAuth("signup")}
            >
              Register
            </ButtonQuizfer>
          </div>
          <AuthTemplate type={typeAuth} handleSubmit={handleSubmit} />
        </div>
      </div>
      <CloseOutlined
        className="absolute top-3 right-5"
        onClick={() => setOpenAuth(false)}
      />
    </Drawer>
  );
  return (
    <FormikContext.Provider value={formik}>
      <If condition={!isAuthenticated}>
        <Then>
          <div className="flex gap-3">
            <ButtonQuizfer
              // className="text-white shadow-none"
              onClick={() => {
                setTypeAuth("signin");
                setOpenAuth(true);
              }}
            >
              Signin
            </ButtonQuizfer>
            <ButtonQuizfer
              className="py-2"
              color="yellow"
              onClick={() => {
                setTypeAuth("signup");
                setOpenAuth(true);
              }}
            >
              Register
            </ButtonQuizfer>
          </div>
        </Then>
        <Else>
          <div className="flex gap-3 items-center relative">
            {/* <span className="mr-2 font-bold whitespace-nowrap">
              {userInfo?.full_name || "..."}
            </span> */}
            <BellFilled className="text-2xl text-white" />
            <Dropdown
              className="font-bold"
              menu={{ items: ItemSetting }}
              trigger={["click"]}
            >
              <div onClick={(e) => e.preventDefault()}>
                <Avatar
                  className="w-10 h-10 cursor-pointer"
                  src="https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80"
                />
              </div>
            </Dropdown>
          </div>
        </Else>
      </If>
      {renderAuthTemplate}
    </FormikContext.Provider>
  );
}

export default AuthNavbar;
