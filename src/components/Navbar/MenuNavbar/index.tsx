import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "./style.scss";
import {
  DatabaseOutlined,
  HomeOutlined,
  IdcardOutlined,
} from "@ant-design/icons";
import { AuthContext } from "config/context/auth";
import { useContext } from "react";
const MenuNavbar: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const { isAuthenticated } = useContext(AuthContext);
  const menuCategory = isAuthenticated
    ? [
        {
          name: "Home",
          href: "/",
          icon: <HomeOutlined />,
        },
        {
          name: "Collections",
          href: "/collections",
          icon: <DatabaseOutlined />,
        },
        {
          name: "Class",
          href: "/class/2",
          icon: <IdcardOutlined />,
        },
      ]
    : [];

  return (
    <div className="category min-h-[8vh] flex items-center gap-10 text-white">
      {menuCategory.map((item, index) => (
        <div
          key={index}
          role="button"
          className={`${
            item.href.length === 1 &&
            location.pathname.length === 1 &&
            "text-yellow"
          } ${
            item.href.length > 1 && location.pathname.includes(item.href)
              ? "text-yellow"
              : ""
          } category__item min-h-[8vh] relative flex items-center font-bold text-md`}
          onClick={() => navigate(item.href)}
        >
          <div className="text-xl mr-2">{item.icon}</div>
          <span>{item.name}</span>
          <div
            className={`${
              item.href.length === 1 && location.pathname.length === 1 && "h-1"
            } ${
              item.href.length > 1 && location.pathname.includes(item.href)
                ? "h-1"
                : ""
            } active-border absolute bottom-0 left-[-10px] right-[-10px] rounded-tr-lg rounded-tl-lg`}
          />
        </div>
      ))}
    </div>
  );
};

export default MenuNavbar;
