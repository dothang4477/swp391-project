import { Avatar } from "antd";
import { useAppSelector } from "reduxHook";
import ButtonQuizfer from "components/ui/button";
import {
  AimOutlined,
  CalendarOutlined,
  EditOutlined,
  MailOutlined,
} from "@ant-design/icons";
import dayjs from "dayjs";
import AccountType from "../AccountType/index";
import ProfilePicture from "../ProfilePicture";
import ChangeEmail from "../ChangeYourEmail";
import ChangeUserName from "../ChangeYourUserName/index";
import ChangePassword from "../ChangePassword/index";
import DeleteAccount from "../DeleteAccount/index";

const UserInfo = () => {
  const userInfo = useAppSelector((state) => state.user.userInfo);
  return (
    <>
      <div className="block w-full">
        <div className="my-10">
          <ProfilePicture />
        </div>
        <div className="my-10">
          <AccountType />
        </div>
        <div className="my-10">
          <ChangeEmail />
        </div>
        <div className="my-10">
          <ChangePassword />
        </div>
        <div className="my-10">
          <ChangeUserName />
        </div>
        <div>
          <DeleteAccount />
        </div>
      </div>

      {/* <div>
        <Avatar
          className="w-[250px] h-[250px] mx-auto mb-3"
          src="https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8dXNlcnxlbnwwfHwwfHw%3D&w=1000&q=80"
        />
        <div className="flex flex-col gap-5">
          <div className="flex flex-col gap-2 mb-10">
            <p className="text-2xl font-bold text-center">
              {userInfo?.full_name}
            </p>
            <p>
              <MailOutlined className="mr-2" />
              {userInfo?.email}
            </p>
            <p>
              <CalendarOutlined className="mr-2" />
              {dayjs(userInfo?.date_of_birth).format("DD-MM-YYYY")}
            </p>
            <p>
              <AimOutlined className="mr-2" />
              {userInfo?.user_name}
            </p>
          </div>
          <ButtonQuizfer className="border">
            <EditOutlined className="mr-2" />
            Update profile
          </ButtonQuizfer>
        </div>
      </div> */}
    </>
  );
};

export default UserInfo;
