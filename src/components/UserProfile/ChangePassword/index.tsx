import { Row, Col, Card, Divider } from "antd";
import ButtonQuizfer from "components/ui/button";
import { SettingOutlined } from "@ant-design/icons";
import InputQuizfer from "components/ui/input";

export const ChangePassword = () => {
  return (
    <>
      <Row className="justify-center gap-10 items-center">
        <div className="contents">
          <Col span={3} className="text-center content-center">
            <SettingOutlined className="text-center text-6xl" />
            <p className="text-center font-bold text-3xl">
              Change your password
            </p>
          </Col>
        </div>
        <div className="contents w-full">
          <Col>
            <Card className="bg-indigo-100 bg-opacity-80 cursor-pointer min-h-[300px] max-w-[900px] px-5 pt-3 pb-[70px] h-full font-bold shadow-xl">
              <p className="font-bold text-2xl my-3">Change your password</p>
              <Divider className="shadow-lg border-gray" />
              <div className="flex flex-wrap gap-3 max-w-[900px] h-auto my-5">
                <InputQuizfer
                  required={true}
                  type="password"
                  placeholder="Your current password"
                  className="w-full shadow-lg"
                />
                <span className="font-light text-sm text-amber-700 italic mt-[-10px]">
                  Current Password
                </span>
                <InputQuizfer
                  required={true}
                  type="password"
                  placeholder="Your new password"
                  className="w-full shadow-lg"
                />
                <span className="font-light text-sm text-amber-700 italic mt-[-10px]">
                  New Password
                </span>
                <InputQuizfer
                  required={true}
                  type="password"
                  placeholder="Confirm your password"
                  className="w-full shadow-lg"
                />
                <span className="font-light text-sm text-amber-700 italic mt-[-10px]">
                  Confirm Password
                </span>
              </div>
              <div className="absolute bottom-3">
                <ButtonQuizfer color="blue">
                  Save
                </ButtonQuizfer>
              </div>
              <div>
                <p>
                  If you forgot your password, you can
                  <a className="text-fuchsia-600" href="/">
                    {" "}
                    Reset your password.
                  </a>
                </p>
              </div>
            </Card>
          </Col>
        </div>
      </Row>
    </>
  );
};

export default ChangePassword;
