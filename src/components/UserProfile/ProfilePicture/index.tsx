import { Row, Col, Card } from "antd";
import ButtonQuizfer from "components/ui/button";

export const ProfilePicture = () => {
  return (
    <>
      <Row className="justify-center gap-10 items-center">
        <div className="contents">
          <Col span={3} className="text-center content-center">
            <img
              width={360}
              src="https://i0.wp.com/thatnhucuocsong.com.vn/wp-content/uploads/2022/09/avatar-anime-1.jpg?ssl=1"
              alt="avatar"
              className="rounded-full"
            />
            <p className="text-center font-bold text-3xl">Profile Picture</p>
          </Col>
        </div>
        <div className="contents w-full">
          <Col>
            <Card className="bg-indigo-100 bg-opacity-80 cursor-pointer min-h-[300px] min-w-[900px] px-5 pt-3 pb-[50px] h-full font-bold shadow-xl">
              <p className="font-bold text-2xl">Choose your profile picture</p>
              <div className="flex flex-wrap gap-3 max-w-[900px] h-auto">
                <img
                  width={60}
                  src="https://i0.wp.com/thatnhucuocsong.com.vn/wp-content/uploads/2022/09/avatar-anime-1.jpg?ssl=1"
                  alt="avatar"
                  className="rounded-full"
                />
              </div>
              <div className="absolute bottom-5">
                <ButtonQuizfer color="blue">
                  Save
                </ButtonQuizfer>
              </div>
            </Card>
          </Col>
        </div>
      </Row>
    </>
  );
};

export default ProfilePicture;
