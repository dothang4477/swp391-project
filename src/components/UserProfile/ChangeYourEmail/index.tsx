import { Row, Col, Card, Divider } from "antd";
import ButtonQuizfer from "components/ui/button";
import { MailOutlined } from "@ant-design/icons";
import InputQuizfer from "components/ui/input";

export const ChangeEmail = () => {
  return (
    <>
      <Row className="justify-center gap-10 items-center">
        <div className="contents">
          <Col span={3} className="text-center content-center">
            <MailOutlined className="text-center text-6xl" />
            <p className="text-center font-bold text-3xl">Change your email</p>
          </Col>
        </div>
        <div className="contents w-full">
          <Col>
            <Card className="bg-indigo-100 bg-opacity-80 cursor-pointer min-h-[300px] min-w-[900px] px-5 pt-3 pb-[70px] h-full font-bold shadow-xl">
              <p className="font-bold text-2xl my-3">
                Update your email address?
              </p>
              <p className="text-lg">Your current email: "Email của bạn"</p>
              <Divider className="shadow-lg border-gray" />
              <div className="flex flex-wrap gap-3 max-w-[900px] h-auto my-5">
                <InputQuizfer
                  required={true}
                  type="email"
                  placeholder="Your new email"
                  className="w-full shadow-lg"
                />
                <span className="font-light text-sm text-amber-700 italic mt-[-10px]">
                  New Email
                </span>

                <InputQuizfer
                  required={true}
                  type="password"
                  placeholder="Your quizfer password"
                  className="w-full shadow-lg"
                />
                <span className="font-light text-sm text-amber-700 italic mt-[-10px]">
                  QuizFer Password
                </span>
              </div>
              <div className="absolute bottom-3">
                <ButtonQuizfer color="blue">
                  Save
                </ButtonQuizfer>
              </div>
              <div>
                <p>
                  If you forgot your password, you can
                  <a className="text-fuchsia-600" href="/">
                    {" "}
                    Reset your password.
                  </a>
                </p>
              </div>
            </Card>
          </Col>
        </div>
      </Row>
    </>
  );
};

export default ChangeEmail;
