import { Row, Col, Card, RadioChangeEvent, Radio, Space } from "antd";
import ButtonQuizfer from "components/ui/button";
import { AuditOutlined } from "@ant-design/icons";
import { useState } from "react";

export const AccountType = () => {
  const [value, setValue] = useState(1);
  const onChange = (e: RadioChangeEvent) => {
    setValue(e.target.value);
  };

  return (
    <>
      <Row className="justify-center gap-10 items-center">
        <div className="contents">
          <Col span={3} className="text-center content-center">
            <AuditOutlined className="text-center text-6xl" />
            <p className="text-center font-bold text-3xl">Account Type</p>
          </Col>
        </div>
        <div className="contents w-full">
          <Col>
            <Card className="bg-indigo-100 bg-opacity-80 cursor-pointer min-h-[300px] min-w-[900px] px-5 pt-3 pb-[50px] h-full font-bold shadow-xl">
              <p className="font-bold text-2xl my-3">
                Teacher or Student account?
              </p>
              <p>Select your account type:</p>
              <div className="flex flex-wrap gap-3 max-w-[900px] h-auto my-5">
                <Radio.Group onChange={onChange} value={value}>
                  <Space direction="vertical">
                    <Radio className="font-bold text-xl" value={1}>
                      Teacher
                    </Radio>
                    <Radio className="font-bold text-xl" value={2}>
                      Student
                    </Radio>
                  </Space>
                </Radio.Group>
              </div>
              <div className="absolute bottom-5">
                <ButtonQuizfer color="blue">
                  Save
                </ButtonQuizfer>
              </div>
            </Card>
          </Col>
        </div>
      </Row>
    </>
  );
};

export default AccountType;
