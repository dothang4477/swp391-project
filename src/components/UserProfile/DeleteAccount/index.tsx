import { Row, Col, Card, Divider } from "antd";
import ButtonQuizfer from "components/ui/button";
import { DeleteOutlined } from "@ant-design/icons";

export const DeleteAccount = () => {
  return (
    <>
      <Row className="justify-center gap-10 items-center">
        <div className="contents">
          <Col span={3} className="text-center content-center">
            <DeleteOutlined className="text-center text-6xl" />
            <p className="text-center font-bold text-3xl">Delete Account</p>
          </Col>
        </div>
        <div className="contents w-full">
          <Col>
            <Card className="bg-indigo-100 bg-opacity-80 cursor-pointer min-h-[150px] min-w-[900px] px-5 pt-3 pb-[40px] h-full font-bold shadow-xl">
              <p className="font-bold text-2xl my-3">
                Permanently delete "username"
              </p>
              <p className="text-lg">
                Careful! This will delete all of your data and cannot be undo.
              </p>
              <Divider className="shadow-lg border-gray" />
              <div className="absolute bottom-3">
                <ButtonQuizfer
                  color="red"
                  className="text-slate-50 hover:shadow-xl"
                >
                  Delete
                </ButtonQuizfer>
              </div>
            </Card>
          </Col>
        </div>
      </Row>
    </>
  );
};

export default DeleteAccount;
