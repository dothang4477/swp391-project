import React from "react";
import { Col, Divider, Drawer, Progress, Row } from "antd";
import ButtonQuizfer from "components/ui/button";
import { CurrentRound } from "store/flashcard/types";
import { ArrowRightOutlined } from "@ant-design/icons";
const completedRoundLogo = require("images/CompleteLearnRound.png");
interface INextRoundDrawer {
  open: boolean;
  itemsInCurrentRound?: CurrentRound[];
  onNext?: () => void;
}

const NextRoundDrawer: React.FC<INextRoundDrawer> = ({
  open,
  onNext,
  itemsInCurrentRound,
}) => {
  return (
    <Drawer
      placement="top"
      closable={false}
      open={open}
      height="100%"
      getContainer={false}
    >
      <div>
        <div className="flex justify-between items-center p-10 w-2/3 mx-auto">
          <img
            width={150}
            className="h-[150px]"
            src={completedRoundLogo}
            alt=""
          />
          <p className="text-3xl font-bold mb-3">
            You're good, keep your leaning
          </p>
          <div className="text-center">
            <Progress
              type="circle"
              percent={10}
              strokeColor="#60c164"
              className="mb-3"
            />
            <p className="text-lg font-bold text-green">Completed 5/50 terms</p>
          </div>
        </div>
      </div>
      <div className="bg-zinc-50 h-[80%]">
        <div className="p-10 w-2/3 mx-auto">
          <div className="flex items-center justify-between mb-5">
            <p className="text-xl font-bold">
              Terminology learned in this round
            </p>
            <ButtonQuizfer color="purple" onClick={onNext}>
              <div className="flex items-center">
                Next Round
                <ArrowRightOutlined className="text-xl ml-3" />
              </div>
            </ButtonQuizfer>
          </div>
          {itemsInCurrentRound?.map((item) => (
            <Row hidden={item.retry} className="bg-white items-center px-5 py-3 rounded-lg shadow-md text-lg font-bold mb-3">
              <Col span={10}>{item.flashcard_data.term}</Col>
              <Divider type="vertical" className="mx-10 bg-slate-600" />
              <Col span={10}>
                {item.flashcard_data.definitions.find((item) => item.is_correct)
                  ?.definition || ""}
              </Col>
            </Row>
          ))}
        </div>
      </div>
    </Drawer>
  );
};

export default NextRoundDrawer;
