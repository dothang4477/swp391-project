import { CloseOutlined, SyncOutlined } from "@ant-design/icons";
import FlashCardLearn from "components/FlashCardSet/Learn/CardLearn";
import ButtonQuizfer from "components/ui/button";
import { useEffect, useMemo, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useAppDispatch } from "reduxHook";
import {
  nextRoundLearn,
  startLearnFlashCard,
} from "store/flashcard/flashCardSlice";
import { CurrentRound, IDefinition } from "store/flashcard/types";
import NextRoundDrawer from "./NextRoundDrawer";

const LearnFlashCardSet = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { id } = useParams();
  const [currentTerm, setCurrentTerm] = useState<number>(0);
  const [listFlashcard, setListFlashCard] = useState<CurrentRound[]>([]);
  const [alertErrorDisplay, setAlertErrorDisplay] = useState<number>(0);
  const [processBar, setProcessBar] = useState<number>(0);
  const [sessionId, setSessionId] = useState<number>(0);
  const [isOpenNextRound, setIsOpenNextRound] = useState<boolean>(false);
  const [listIdFlashCard, setlistIdFlashCard] = useState<number[]>([]);

  useEffect(() => {
    document.title = `Learn Sample | Quizfer`;
    fetch();
  }, [id]);

  const fetch = () => {
    if (id) {
      dispatch(startLearnFlashCard(id)).then(({ payload }: any) => {
        setListFlashCard(payload.current_round);
        setSessionId(payload.learn_session.session_id)
        setlistIdFlashCard(payload.current_round.map((item) => item.learn_id));
        setCurrentTerm(0)
      });
    }
  };

  const submitAnswer = (answerObj: IDefinition) => {
    if (!answerObj.is_correct) {
      setAlertErrorDisplay(1);
      listFlashcard.push({ ...listFlashcard[currentTerm], retry: true });
      return;
    }
    setProcessBar((v) => v + 100 / listFlashcard.length);
    setTimeout(() => {
      if (!listFlashcard[currentTerm + 1]?.flashcard_data) {
        dispatch(
          nextRoundLearn({
            payload: {
              incorrect: [],
              correct: listIdFlashCard,
            },
            session_id: sessionId,
          })
        );
        setProcessBar(0);
        setIsOpenNextRound(true);
        return;
      }
      setCurrentTerm((v) => v + 1);
    }, 1000);
  };

  const onNextRound = () => {
    fetch()
    setIsOpenNextRound(false);
  };

  return (
    <div className="bg-rose-light min-h-screen">
      <div className="relative z-20">
        <div className="bg-white w-full flex justify-center items-center shadow-md h-[8vh] text-lg font-bold ">
          <div className="flex items-center absolute left-5">
            <SyncOutlined className="mr-2 text-2xl text-purple" />
            Learn
          </div>
          <div>Round 1/10</div>
          <div
            role="button"
            className="flex items-center absolute right-5 px-3 py-2 shadow-md rounded-lg hover:scale-105"
            onClick={() => navigate(-1)}
          >
            <CloseOutlined />
          </div>
        </div>
        <div
          style={{
            transition: "width 1s",
          }}
          className={`h-[2px] w-[${processBar}%] bg-purple absolute bottom-0`}
        />
      </div>

      <div className="h-[91vh] relative overflow-hidden z-10">
        <div className="px-[25%]  mt-10">
          <FlashCardLearn
            idxFlashCard={currentTerm}
            isRetry={listFlashcard[currentTerm]?.retry}
            termText={listFlashcard[currentTerm]?.flashcard_data?.term || ""}
            answers={
              listFlashcard[currentTerm]?.flashcard_data?.definitions || []
            }
            onSubmitAnswer={submitAnswer}
          />
        </div>
        <div
          style={{
            opacity: alertErrorDisplay,
            transition: "opacity 0.2s linear",
          }}
          className="flex justify-around items-center absolute bottom-0 w-full p-5 bg-white"
        >
          <p className="text-lg text-gray font-bold">
            Try to remember this term
          </p>
          <ButtonQuizfer
            color="purple"
            onClick={() => {
              setCurrentTerm((v) => v + 1);
              setAlertErrorDisplay(0);
            }}
          >
            I got it
          </ButtonQuizfer>
        </div>
        <NextRoundDrawer
          itemsInCurrentRound={listFlashcard}
          open={isOpenNextRound}
          onNext={onNextRound}
        />
      </div>
    </div>
  );
};
export default LearnFlashCardSet;
