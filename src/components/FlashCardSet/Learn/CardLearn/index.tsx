import { FC, useEffect, useState } from "react";
import AnswerButton from "components/ui/AnswerButton";
import { Card, Tag } from "antd";
import { IDefinition } from "store/flashcard/types";
interface IFlashCardProps {
  idxFlashCard?: number;
  isRetry?: boolean;
  termText: string | any;
  answers?: IDefinition[];
  onSubmitAnswer?: (answerObj: IDefinition) => void;
}
const FlashCardLearn: FC<IFlashCardProps> = ({
  idxFlashCard,
  termText,
  answers,
  isRetry,
  onSubmitAnswer,
}) => {
  const [answerPicked, setAnswerPicked] = useState("");

  useEffect(() => {
    setAnswerPicked("");
  }, [idxFlashCard]);

  const onAnswer = (value: string, answerObj: IDefinition) => {
    setAnswerPicked(value);
    onSubmitAnswer && onSubmitAnswer(answerObj);
  };

  return (
    <Card className=" text-slate-600 rounded-2xl px-[5%] py-5 shadow-lg font-bold">
      <div className="min-h-[350px] mb-[30px] flex flex-col justify-between">
        <div>
          <Tag hidden={!isRetry} color="gold" className="text-base border-none mb-3">
            Try again
          </Tag>
          <p className="text-xl text-left">
            <span className="text-lg text-gray mr-2">Term:</span>
            {termText}
          </p>
        </div>
        <div>
          <p className="text-lg text-gray mb-2">Pick your answer</p>
          <div className="grid grid-cols-2 gap-5">
            {answers?.map((defi, index) => (
              <div key={index}>
                <AnswerButton
                  onClick={(v) => onAnswer(v, defi)}
                  answerText={defi.definition}
                  status={
                    answerPicked && defi.is_correct
                      ? "success"
                      : answerPicked === defi.definition && !defi.is_correct
                      ? "failed"
                      : ""
                  }
                  disabled={!!answerPicked && answerPicked !== defi.definition}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </Card>
  );
};
export default FlashCardLearn;
