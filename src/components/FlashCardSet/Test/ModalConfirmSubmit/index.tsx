import { DeliveredProcedureOutlined } from "@ant-design/icons";
import { Modal, ModalFuncProps } from "antd";
import ButtonQuizfer from "components/ui/button";
import { FC } from "react";
import "./style.scss"
const ModalComfirmSubmitTest: FC<ModalFuncProps> = (props) => {
  return (
    <Modal
      className="comfirm-submit text-center"
      title={props?.title || "Submit Comfirm"}
      open={props.open}
      onCancel={props.onCancel}
    >
      <div className="py-3">
        <p className="text-base mb-10">
          Do you want to submit now?
        </p>
        <div className="flex gap-6 justify-center">
          <ButtonQuizfer color="green" onClick={props.onOk}>
           <div className="flex items-center gap-3">
            Submit
           <DeliveredProcedureOutlined />
           </div>
          </ButtonQuizfer>
          <ButtonQuizfer className="text-gray-dark" onClick={props.onCancel}>
            Cancel
          </ButtonQuizfer>
        </div>
      </div>
    </Modal>
  );
};
export default ModalComfirmSubmitTest;
