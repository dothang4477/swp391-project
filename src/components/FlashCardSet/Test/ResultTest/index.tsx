import { Tabs } from "antd";
import { Pie } from "@ant-design/plots";
import { memo } from "react";
import TabPane from "antd/es/tabs/TabPane";
import FlashCardTest from "components/FlashCardSet/Test/CardTest";
import {
  CheckOutlined,
  CloseOutlined,
} from "@ant-design/icons";

const ResultTestPage = ({ result }:any) => {
  const data = [
    {
      type: "Incorrect",
      value: 4,
    },
    {
      type: "Correct",
      value: 1,
    },
  ];
  const config: any = {
    data,
    width: 200,
    height: 200,
    angleField: "value",
    colorField: "type",
    radius: 1,
    innerRadius: 0.6,
    legend: false,
    statistic: {
      title: false,
      content: {
        style: {
          whiteSpace: "pre-wrap",
          overflow: "hidden",
          textOverflow: "ellipsis",
          fontSize: "18px",
          color: "red",
        },
        content: "Not passed",
      },
    },
    color: ["#ff1f21", "#43c361"],
  };
  return (
    <div>
      <div className="mx-[25%] flex justify-between mb-5">
        <div>
          <p className="text-3xl font-bold mb-3">Result of your test</p>
          <div className="font-bold text-xl">
            <p className="text-green">Correct: 1/5</p>
            <p className="text-red-600">Incorrect: 4/5</p>
          </div>
        </div>
        <div>
          <Pie {...config} />
        </div>
      </div>
      <div>
        <p className="text-3xl font-bold mb-3">Description</p>
        <Tabs defaultActiveKey="0" tabPosition="left">
          {result.map((item, idx) => (
            <TabPane
              key={idx}
              tab={
                <div className={`font-bold ${idx === 1 ? "text-green" :"text-red-600"} `}>
                  Question {idx + 1}
                  {idx === 1 ? <CheckOutlined className="ml-2" /> : <CloseOutlined className="ml-2" />  }
                </div>
              }
            >
              <div className="mx-[10%]">
                <FlashCardTest
                  termText={item.term}
                  answers={item.definitions}
                  disableAnswer
                />
              </div>
            </TabPane>
          ))}
        </Tabs>
      </div>
    </div>
  );
};

export default memo(ResultTestPage);
