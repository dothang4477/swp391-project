import { FC, useEffect, useState } from "react";
import TabPane from "antd/es/tabs/TabPane";
import {
  CheckOutlined,
  ClockCircleOutlined,
  CloseOutlined,
  DeliveredProcedureOutlined,
  SnippetsOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { Tabs } from "antd";
import FlashCardTest from "components/FlashCardSet/Test/CardTest";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { useAppDispatch } from "reduxHook";
import { getTestFlashCard } from "store/flashcard/flashCardSlice";
import { IDefinition, IFlashCard } from "store/flashcard/types";
import ButtonQuizfer from "components/ui/button";
import dayjs from "dayjs";
import ModalComfirmSubmitTest from "./ModalConfirmSubmit";
import { Else, If, Then } from "react-if";
import ResultTestPage from "./ResultTest";

const TestFlashCardSet = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  const { id } = useParams();
  const [timer, setTimer] = useState<string>(
    dayjs(new Date()).format("HH:mm:ss")
  );
  const [listTest, setListTest] = useState<IFlashCard[]>([]);
  const [confirmSubmit, setConfirmSubmit] = useState<boolean>(false);
  const [showResult, setShowResult] = useState<boolean>(false);
  const [listSubmitTest, setlistSubmitTest] = useState<
    { flashCardId: number; answer: IDefinition }[]
  >([]);

  useEffect(() => {
    if (id) {
      dispatch(
        getTestFlashCard({
          flashCardSetId: +id,
          numberQuestion: Number(query.get("numberOfQuestion")),
        })
      ).then(({ payload: data }: any) => {
        setListTest(data);
        setInterval(() => {
          setTimer(dayjs(new Date()).format("HH:mm:ss"));
        }, 1000);
      });
    }
  }, []);

  const handleAddAnswer = (answer: IDefinition, id: number) => {
    const findItem = listSubmitTest.find((item) => item.flashCardId === id);
    const newList = listSubmitTest.map((item) => {
      if (item.flashCardId === findItem?.flashCardId) {
        item.answer = answer;
      }
      return item;
    });
    if (findItem) {
      setlistSubmitTest(newList);
      return;
    }
    setlistSubmitTest([...listSubmitTest, { flashCardId: id, answer }]);
  };

  const checkIsMarkedAnswer = (flashCardId: number) => {
    return listSubmitTest.find((item) => item.flashCardId === flashCardId);
  };

  const handleSubmitTest = () => {
    setConfirmSubmit(false);
    setShowResult(true);
  };

  return (
    <>
      <div className="relative z-20">
        <div className="bg-white w-full flex justify-center items-center shadow-md h-[8vh] text-lg font-bold">
          <div className="flex gap-2 items-center absolute left-5">
            <SnippetsOutlined className="text-2xl  text-purple" />
            Test
          </div>
          <div className="flex items-center gap-3">
            Now: {timer}
            <ClockCircleOutlined className="text-2xl" />
          </div>
          <div
            role="button"
            className="flex items-center absolute right-5 px-3 py-2 shadow-md rounded-lg hover:scale-105"
            onClick={() => navigate(-1)}
          >
            <CloseOutlined />
          </div>
        </div>
      </div>
      <If condition={showResult}>
        <Then>
          <div className="w-2/3 mx-auto py-10">
            <ResultTestPage result={listTest} />
          </div>
        </Then>
        <Else>
          <div className="py-10 mx-[10%]">
            <Tabs
              className="h-full"
              tabBarExtraContent={
                <ButtonQuizfer
                  color="green"
                  className="shadow-lg mr-5"
                  disabled={
                    !listTest.length ||
                    listSubmitTest.length !== listTest.length
                  }
                  onClick={() => setConfirmSubmit(true)}
                >
                  <div className="flex items-center gap-3">
                    Submit
                    <DeliveredProcedureOutlined className="text-2xl" />
                  </div>
                </ButtonQuizfer>
              }
              defaultActiveKey="1"
              tabPosition="left"
            >
              {listTest.map((item, idx) => (
                <TabPane
                  key={idx}
                  tab={
                    <div
                      className={`font-bold ${
                        checkIsMarkedAnswer(item.id as number)
                          ? "text-gray-dark"
                          : "text-slate-300"
                      } `}
                    >
                      Question {idx + 1}
                      {/* <CheckOutlined /> */}
                    </div>
                  }
                >
                  <div className="mx-[10%]">
                    <FlashCardTest
                      termText={item.term}
                      answers={item.definitions}
                      onSubmitAnswer={(value) =>
                        handleAddAnswer(value, item.id as number)
                      }
                    />
                  </div>
                </TabPane>
              ))}
            </Tabs>
          </div>
          <ModalComfirmSubmitTest
            open={confirmSubmit}
            onOk={handleSubmitTest}
            onCancel={() => setConfirmSubmit(false)}
          />
        </Else>
      </If>
    </>
  );
};
export default TestFlashCardSet;
