import { FC, useEffect, useState } from "react";
import AnswerButton from "components/ui/AnswerButton";
import { Card, Tag } from "antd";
import { IDefinition } from "store/flashcard/types";
interface IFlashCardProps {
  idxFlashCard?: number;
  termText: string | any;
  answers?: IDefinition[];
  disableAnswer?: boolean;
  onSubmitAnswer?: (answerObj: IDefinition) => void;
}
const FlashCardLearn: FC<IFlashCardProps> = ({
  idxFlashCard,
  termText,
  answers,
  disableAnswer,
  onSubmitAnswer,
}) => {
  const [answerPicked, setAnswerPicked] = useState("");

  useEffect(() => {
    setAnswerPicked("");
  }, [idxFlashCard]);

  const onAnswer = (value: string, answerObj: IDefinition) => {
    setAnswerPicked(value);
    onSubmitAnswer && onSubmitAnswer(answerObj);
  };

  return (
    <Card className=" text-slate-600 rounded-2xl px-[5%] py-5 shadow-lg font-bold">
      <div className="min-h-[350px] mb-[30px] flex flex-col justify-between">
        <p className="text-xl text-left">
          <span className="text-lg text-gray mr-2">Term:</span>
          {termText}
        </p>
        <div>
          <p className="text-lg text-gray mb-2">Pick your answer</p>
          <div className="grid grid-cols-2 gap-5">
            {answers?.map((defi, index) => (
              <div key={index}>
                <AnswerButton
                  onClick={(v) => onAnswer(v, defi)}
                  answerText={defi.definition}
                  status={
                    disableAnswer
                      ? defi.is_correct
                        ? "success"
                        : "failed"
                      : answerPicked === defi.definition
                      ? "selected"
                      : ""
                  }
                  disabled={disableAnswer && !defi.is_correct}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </Card>
  );
};
export default FlashCardLearn;
