import * as yup from "yup";
import { useFormik } from "formik";
import { useAppDispatch } from "reduxHook";
import { useContext } from "react";
import { NotificationContext } from "config/context/Notification";
import { IFlashCard } from "../types";
import {
  addMyNewFlashCardSet,
  editFlashCardSet,
} from "store/flashcard/flashCardSlice";
import { useNavigate } from "react-router-dom";
export interface IValuesTypes {
  set_name: string;
  description: string;
  tag_name?: string[];
  flashcards: IFlashCard[];
  flash_card_set_id?: number | string;
}

const initialValues = {
  set_name: "",
  description: "",
  tag_name: [],
  flashcards: [],
  flash_card_set_id: 0,
};

const validationSchema = yup.object().shape({
  set_name: yup.string().required("This field is required"),
  flashcards: yup
    .array()
    .of(
      yup.object().shape({
        term: yup.string().required("This field is required"),
        definitions: yup
          .array()
          .of(yup.string().required("Anwser is required"))
          .required("At least one anwser is required!"),
      })
    )
    .required("Required least 1 term!"),
});

export const useCreateFlashCardSet = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { notification } = useContext(NotificationContext);
  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values) => {
      if (values.flash_card_set_id > 0) {
        const cookValue = {
          ...values,
          flashcards: values.flashcards.map((item: IFlashCard) => ({
            term: item.term as string,
            definitions: item.definitions.map((item) => ({
              definition: item,
            })),
          })),
        };
        const { payload: data } = await dispatch(editFlashCardSet(cookValue));
        if (data) {
          notification("success", "Update flashcard set successfully!");
        } else {
          notification("error", "Update flashcard set failed!");
        }
        return;
      }
      const { payload: data } = await dispatch(addMyNewFlashCardSet(values));
      if (data) {
        notification("success", "Create new flashcard set successfully!");
        navigate("/collections");
      } else {
        notification("error", "Create new flashcard set failed!");
      }
    },
  });

  const handleSubmit = () => {
    formik.handleSubmit();
  };

  return {
    formik,
    handleSubmit,
  };
};
