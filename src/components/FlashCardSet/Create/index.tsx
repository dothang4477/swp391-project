import {
  DiffFilled,
  PictureFilled,
  PlusCircleFilled,
  PlusCircleOutlined,
  MinusCircleFilled,
} from "@ant-design/icons";
import { Affix, Card, Col, Divider, Row, Tag } from "antd";
import TextArea from "antd/es/input/TextArea";
import ButtonQuizfer from "components/ui/button";
import InputQuizfer from "components/ui/input";
import { useEffect, useState } from "react";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { useCreateFlashCardSet } from "./hooks";
import { IFlashCard } from "./types";
import { FormikErrors } from "formik";
import { IFlashCardPayload } from "store/flashcard/types";
import { useParams } from "react-router-dom";

interface ICreateFlashCardSetProps {
  flashCardSetInfo?: any;
  flashcardsDefault?: IFlashCardPayload[];
}

const CreateFlashCardSet: React.FC<ICreateFlashCardSetProps> = ({
  flashCardSetInfo = null,
  flashcardsDefault,
}) => {
  const {
    formik: { values, errors, setFieldValue, handleSubmit },
  } = useCreateFlashCardSet();
  const { id } = useParams();
  const [isSubmit, setIsSubmit] = useState(false);
  const [inputTag, setInputTag] = useState("");
  const [flashcards, setFlashcards] = useState<IFlashCardPayload[]>([
    {
      term: "",
      definitions: ["", ""],
    },
  ]);

  useEffect(() => {
    if (flashcardsDefault?.length && flashCardSetInfo) {
      setFieldValue("set_name", flashCardSetInfo.set_name);
      setFieldValue("description", flashCardSetInfo.description);
      setFieldValue("tag_name", flashCardSetInfo.tag_name);
      setFieldValue("flash_card_set_id", Number(id));
      setFlashcards(flashcardsDefault);
    }
  }, [flashcardsDefault, flashCardSetInfo]);

  useEffect(() => {
    setFieldValue("flashcards", flashcards);
  }, [flashcards, setFieldValue]);

  const handleSetDefinition = (
    indexTerm: number,
    indexAnswer: number,
    value: string
  ) => {
    const newList = flashcards.map((term, index) => {
      if (index === indexTerm) {
        term.definitions[indexAnswer] = value;
      }
      return term;
    });
    setFlashcards(newList);
  };

  const onAddNewTerm = () => {
    setFlashcards([
      ...flashcards,
      {
        term: "",
        definitions: [""],
      },
    ]);
  };

  const onAddNewAnswer = (indexTerm: number) => {
    let newList = [...flashcards];
    newList[indexTerm].definitions.push("");
    setFlashcards(newList);
  };

  const onDeleteTerm = (indexDelete: number) => {
    let newList = [...flashcards];
    newList.splice(indexDelete, 1);
    setFlashcards(newList);
  };

  const onDeleteAnswer = (indexTerm: number, indexAnswer: number) => {
    let newList = [...flashcards];
    newList[indexTerm].definitions.splice(indexAnswer, 1);
    setFlashcards(newList);
  };
  const onDeleteTag = (indexTag: number) => {
    let newList = [...values.tag_name];
    newList.splice(indexTag, 1);
    setFieldValue("tag_name", newList);
  };

  const errorMessageDefiniton = (
    obj: any,
    indexDefi: number
  ): obj is FormikErrors<IFlashCard> => {
    return obj?.definitions?.[indexDefi];
  };
  const errorMessageTerm = (obj: any): obj is FormikErrors<IFlashCard> => {
    return obj?.term;
  };

  return (
    <div className="container mx-auto">
      <p className="font-bold text-2xl text-center text-purple">
        Title is coming
      </p>
      <Divider className="my-7 bg-purple" />
      <Row className="justify-between px-5 gap-10 relative">
        <Col span={8}>
          <Affix offsetTop={110}>
            <Card className="col-span-3 shadow-md p-5 rounded-2xl">
              <p className="text-center font-bold text-2xl mb-3">Information</p>
              <div className="flex flex-col gap-3">
                <InputQuizfer
                  label="Name"
                  required
                  value={values.set_name}
                  error={isSubmit && errors.set_name}
                  onChange={(value) => setFieldValue("set_name", value.trim())}
                />
                <div>
                  <p className="font-bold mb-2 text-lg  ">Description</p>
                  <TextArea
                    autoSize
                    value={values.description}
                    placeholder="Enter description"
                    className={`border-b-2 border-b-purple ${
                      errors.description && isSubmit && "border-red-500"
                    }`}
                    onChange={(e) =>
                      setFieldValue("description", e.target.value.trim())
                    }
                  />
                  {errors.description && isSubmit && (
                    <span className="text-sm text-red-500">
                      {errors.description}
                    </span>
                  )}
                </div>
                <InputQuizfer
                  value={inputTag}
                  className="mb-3"
                  label="Tags"
                  onChange={setInputTag}
                  onEnter={() => {
                    if (!values.tag_name.includes(inputTag as never)) {
                      setFieldValue("tag_name", [...values.tag_name, inputTag]);
                    }
                    setInputTag("");
                  }}
                />
                <div className="text-blue font-bold">
                  {values.tag_name.map((item, index) => (
                    <Tag
                      color="blue"
                      className="text-lg"
                      closable
                      onClose={() => onDeleteTag(index)}
                    >
                      #{item}
                    </Tag>
                  ))}
                </div>
                <p className="text-center font-bold   text-xl mb-3">
                  Term: {flashcards.length}
                </p>
                <ButtonQuizfer
                  color="purple"
                  className="border flex items-center text-2xl w-fit h-[55px] mx-auto"
                  onClick={() => {
                    handleSubmit();
                    setIsSubmit(true);
                  }}
                >
                  <DiffFilled className="text-xl mr-3" />
                  <span>{id ? "Update" : "Create"}</span>
                </ButtonQuizfer>
              </div>
            </Card>
          </Affix>
        </Col>

        <Col span={15}>
          <div className="mb-5">
            {flashcards.map((termInfo, index) => (
              <Card
                key={index}
                className=" text-slate-600 rounded-2xl px-10 py-5 shadow-md font-bold mb-5"
              >
                <div className="mb-5">
                  <p className="font-bold mb-2 text-xl  ">
                    Term {index + 1}
                    <span className="ml-2 text-red-600">*</span>
                  </p>
                  <TextArea
                    value={termInfo.term}
                    autoSize
                    className={`border-b-2 border-b-purple ${
                      errorMessageTerm(errors.flashcards?.[index]) &&
                      isSubmit &&
                      "border-b-red-500"
                    }`}
                    placeholder="Enter term description"
                    onChange={(e) => {
                      let newList = [...flashcards];
                      newList[index].term = e.target.value.trim();
                      setFlashcards(newList);
                    }}
                  />
                  {errorMessageTerm(errors.flashcards?.[index]) && isSubmit && (
                    <span className="text-sm text-red-500">
                      {errorMessageTerm(errors.flashcards?.[index])}
                    </span>
                  )}
                </div>
                <div
                  role="button"
                  className="text-center relative w-fit mx-auto p-5 border rounded-lg border-dashed mb-2 hover:text-blue"
                >
                  <PictureFilled className="text-5xl" />
                  <PlusCircleOutlined className="absolute top-[-10px] text-xl bg-white left-[-10px] rounded-full" />
                </div>
                <div>
                  <p className="font-bold mb-2  ">
                    Answer(s)
                    <span className="ml-2 text-red-600">*</span>
                  </p>

                  <div className="grid grid-cols-2 gap-5">
                    {termInfo.definitions.map((answer, indexDefi) => (
                      <div key={indexDefi} className="relative">
                        <div className="relative">
                          <InputQuizfer
                            value={answer}
                            error={
                              isSubmit &&
                              errors.flashcards?.[index] &&
                              errorMessageDefiniton(
                                errors.flashcards?.[index],
                                indexDefi
                              )
                            }
                            className={
                              indexDefi === 0 ? "text-green" : "text-red-600"
                            }
                            placeholder={
                              indexDefi === 0
                                ? "Enter correct answer"
                                : "Enter wrong answer"
                            }
                            onChange={(value) =>
                              handleSetDefinition(
                                index,
                                indexDefi,
                                value.trim()
                              )
                            }
                          />
                          <div className="text-lg absolute top-4 right-4">
                            {indexDefi === 0 ? (
                              <CheckOutlined className="text-green" />
                            ) : (
                              <CloseOutlined className="text-red-600" />
                            )}
                          </div>
                        </div>
                        <MinusCircleFilled
                          className="text-lg absolute top-[-8px] right-[-8px] text-red-500"
                          onClick={() => onDeleteAnswer(index, indexDefi)}
                        />
                      </div>
                    ))}
                    <ButtonQuizfer
                      className="flex items-center h-[55px] border text-green "
                      onClick={() => onAddNewAnswer(index)}
                    >
                      <PlusCircleFilled /> <span> Add</span>
                    </ButtonQuizfer>
                  </div>
                </div>
                <MinusCircleFilled
                  className="text-2xl absolute top-[-10px] right-[-10px] text-red-500"
                  onClick={() => onDeleteTerm(index)}
                />
              </Card>
            ))}
          </div>
          <ButtonQuizfer
            className="flex items-center w-fit h-[55px] mx-auto text-rose-light  bg-green  border border-green hover:text-green"
            onClick={onAddNewTerm}
          >
            <PlusCircleFilled /> <span> Add new term</span>
          </ButtonQuizfer>
        </Col>
      </Row>
    </div>
  );
};

export default CreateFlashCardSet;
