export interface IFlashCardType {
  name?: string;
  numberOfTerm?: number;
  totalLearn?: number;
  authorName: string;
  rating: string;
}
