import {
  CaretRightOutlined,
  CloseOutlined,
  DeleteOutlined,
  EditOutlined,
  FileSyncOutlined,
  SettingOutlined,
  ShareAltOutlined,
  SnippetsOutlined,
} from "@ant-design/icons";
import { Col, Drawer, Rate, Row, Card, Input, Dropdown, MenuProps } from "antd";
import ButtonQuizfer from "components/ui/button";
import { Divider } from "antd";
import { useContext, useEffect, useMemo, useState } from "react";
import { useNavigate, useParams } from "react-router";
import { useAppDispatch, useAppSelector } from "reduxHook";
import {
  deleteFlashCardSet,
  getDetailFlashCardSet,
} from "store/flashcard/flashCardSlice";
import { IFlashCardSetDetail } from "store/flashcard/types";
import CreateFlashCardSet from "../Create";
import DeleteModal from "components/ui/Modal/DeleteModal";
import { NotificationContext } from "config/context/Notification";
import FlipCard from "components/ui/FlipCard";
import SwiperQuizfer from "components/ui/Swiper";
import { Else, If, Then } from "react-if";
const deletedUser = require("images/deleted.png");

const FlashCardSetDetail = () => {
  const navigate = useNavigate();
  let { id } = useParams();
  const dispatch = useAppDispatch();
  const { notification } = useContext(NotificationContext);
  const [isOpenEditSet, setIsOpenEditSet] = useState<boolean>(false);
  const [isOpenDeleteSet, setIsOpenDeleteSet] = useState<boolean>(false);
  const [option, setOption] = useState("flashcard");
  const [numberOfQuestion, setNumberOfQuestion] = useState<number>();

  const isUpdateFlashCardSet = useAppSelector(
    (state) => state.flashCard.isUpdateFlashCardSet
  );
  const isDeleteFlashCardSet = useAppSelector(
    (state) => state.flashCard.isDeleteFlashCardSet
  );
  const [flashCardSetInfo, setFlashCardSetInfo] =
    useState<IFlashCardSetDetail>();

  const optionSetting: MenuProps["items"] = [
    {
      key: "1",
      label: (
        <div
          role="button"
          className="text-lg font-bold px-5"
          onClick={() => setIsOpenEditSet(true)}
        >
          <EditOutlined className="mr-3" />
          Edit
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <div role="button" className="text-lg font-bold px-5">
          <ShareAltOutlined className="mr-3" />
          Share
        </div>
      ),
    },
    {
      key: "3",
      label: (
        <div
          role="button"
          className="text-lg text-red-600 font-bold px-5"
          onClick={() => setIsOpenDeleteSet(true)}
        >
          <DeleteOutlined className="mr-3" />
          Delete
        </div>
      ),
    },
  ];

  useEffect(() => {
    fetchData();
  }, [id]);

  useEffect(() => {
    if (isUpdateFlashCardSet || isDeleteFlashCardSet) {
      setIsOpenEditSet(false);
      fetchData();
    }
  }, [isUpdateFlashCardSet, isDeleteFlashCardSet]);

  const fetchData = () => {
    if (id) {
      dispatch(getDetailFlashCardSet(id)).then(({ payload: data }: any) => {
        if (data) {
          setFlashCardSetInfo(data as IFlashCardSetDetail);
          setNumberOfQuestion(data?.flashcards?.length);
        }
      });
    }
  };

  const handleDeleteSet = async () => {
    if (id) {
      const { payload: data } = await dispatch(deleteFlashCardSet(id));
      if (data) {
        notification("success", "Delete set successfully!");
        navigate(-1);
      } else {
        notification("error", "Can't delete set now!");
      }
      setIsOpenDeleteSet(false);
    }
  };

  const formatEditDataSet = useMemo(() => {
    document.title = `${flashCardSetInfo?.set_name} | Quizfer`;
    if (flashCardSetInfo?.flashcards) {
      return flashCardSetInfo?.flashcards.map((item) => ({
        term: item.term,
        definitions: item.definitions.map((item) => item.definition),
      }));
    }
    return [];
  }, [flashCardSetInfo]);

  return (
    <div className="container mx-auto">
      <Row className="justify-between">
        <Col span={18} className="set-info">
          <h1 className="font-bold text-4xl mb-3">
            {flashCardSetInfo?.set_name}
          </h1>
          <h2 className="text-2xl font-bold mb-3">
            {flashCardSetInfo?.description}
          </h2>
          <Rate
            className="text-base text-right mb-3"
            allowHalf
            defaultValue={flashCardSetInfo?.meta_data?.rate || 0}
          />
          <div className="flex gap-2">
            {flashCardSetInfo?.tag_name?.map((item, index) => (
              <span
                key={index}
                role="link"
                className="text-blue hover:underline"
              >
                #{item}
              </span>
            ))}
          </div>
          <div className="flex items-center gap-2">
            <img width={30} src="" alt="" className="rounded-full" />
            <span className="">{flashCardSetInfo?.author?.name}</span>
          </div>
        </Col>
        <Col span={4} className="flex justify-end items-start gap-3 relative">
          <Dropdown
            trigger={["click"]}
            menu={{ items: optionSetting }}
            placement="bottom"
            arrow
          >
            <SettingOutlined className="text-2xl" />
          </Dropdown>
        </Col>
      </Row>
      <div className="flex justify-between items-center">
        <div className="flex gap-3 mt-5">
          <ButtonQuizfer color="yellow" onClick={() => setOption("flashcard")}>
            <FileSyncOutlined className="mr-2" />
            Flashcards
          </ButtonQuizfer>
          <ButtonQuizfer color="yellow" onClick={() => setOption("test")}>
            <SnippetsOutlined className="mr-2" />
            Test
          </ButtonQuizfer>
        </div>
        <div className="flex gap-4 text-end">
          <div>
            <p>Created by</p>
            <p className="font-bold text-xl">
              {flashCardSetInfo?.author?.firstName}(authorName)
            </p>
          </div>
          <img
            width={50}
            className="rounded-full"
            src={flashCardSetInfo?.author || deletedUser}
            alt=""
          />
        </div>
      </div>
      <Divider className="bg-galaxy-purple shadow-md my-12" />
      <If condition={option === "flashcard"}>
        <Then>
          <div className="flex justify-between items-center">
            <p className="text-2xl font-bold">Preview</p>
            <ButtonQuizfer
              color="yellow"
              onClick={() =>
                navigate(`/flashcard-set/${flashCardSetInfo?.set_id}/learn`)
              }
            >
              Start Learn
              <CaretRightOutlined className="ml-2" />
            </ButtonQuizfer>
          </div>
          <SwiperQuizfer
            slides={
              flashCardSetInfo?.flashcards.map((item, index) => (
                <div className="w-2/3 mx-auto">
                  <FlipCard
                    term={item.term}
                    definition={item.definitions[0].definition}
                    indexCard={`${index + 1}/${
                      flashCardSetInfo.flashcards.length
                    }`}
                  />
                </div>
              )) || []
            }
            options={{
              slidesPerView: 1,
              spaceBetween: 40,
            }}
          />
          <Divider className="bg-galaxy-purple shadow-md my-12" />
          <div>
            <p className="font-bold text-2xl mb-5">
              Preview all
            </p>
            <div>
              {flashCardSetInfo?.flashcards.map((item, index) => (
                <div className="">
                  <Card className="w-2/3 mx-auto h-auto shadow-lg mb-5 rounded-2xl bg-gradient-to-b from-cardfliptop to-cardflipbottom hover:border-2 text-white">
                    <Row className="p-3">
                      <Col flex={2} className="w-[4%] text-xl">
                        {item.definitions[0].definition}
                      </Col>
                      <Divider
                        type="vertical"
                        className="bg-slate-100 h-[auto]"
                      ></Divider>
                      <Col flex={3} className="text-xl w-[20%]">
                        <p>{flashCardSetInfo.flashcards[index].term}</p>
                        <p className="italic font-thin text-sm mt-10">
                          Answer(s)
                        </p>
                        {flashCardSetInfo?.flashcards[index].definitions.map(
                          (item, index) => (
                            <p>
                              {index + 1}.{item.definition}
                            </p>
                          )
                        )}
                      </Col>
                    </Row>
                  </Card>
                </div>
              ))}
            </div>
          </div>
        </Then>
        <Else>
          <Card className="bg-white w-1/2 mx-auto max-h-[250px] p-3 shadow-md rounded-xl">
            <p className="italic text-xl font-bold mb-5 text-center">
              Create Test
            </p>
            <div className=" bg-purple text-white shadow-md rounded-lg p-3 mb-5">
              <p className="text-xl font-bold">{flashCardSetInfo?.set_name}</p>
              <p>{flashCardSetInfo?.flashcards.length} terms</p>
            </div>
            <div className="flex justify-between items-center gap-6 px-5">
              <p className="italic text-xl font-bold whitespace-nowrap">
                Total: {flashCardSetInfo?.flashcards.length}
              </p>
              <Input
                value={numberOfQuestion}
                placeholder="Enter number of question"
                type="number"
                max={flashCardSetInfo?.flashcards.length}
                min={1}
                onChange={(e) => setNumberOfQuestion(+e.target.value)}
              />
              <ButtonQuizfer
                color="yellow"
                onClick={() => {
                  navigate(
                    `/flashcard-set/${flashCardSetInfo?.set_id}/test?numberOfQuestion=${numberOfQuestion}`
                  );
                }}
              >
                <div className="flex items-center gap-2">
                  Start
                  <CaretRightOutlined />
                </div>
              </ButtonQuizfer>
            </div>
          </Card>
        </Else>
      </If>

      <Drawer
        className="p-0"
        closable={false}
        placement={"top"}
        height="100%"
        open={isOpenEditSet}
      >
        <div className="py-5">
          <CreateFlashCardSet
            flashCardSetInfo={{
              set_name: flashCardSetInfo?.set_name,
              description: flashCardSetInfo?.description,
              tag_name: flashCardSetInfo?.tag_name,
            }}
            flashcardsDefault={formatEditDataSet}
          />
          <CloseOutlined
            className="absolute top-3 right-5"
            onClick={() => setIsOpenEditSet(false)}
          />
        </div>
      </Drawer>
      <DeleteModal
        open={isOpenDeleteSet}
        onOk={handleDeleteSet}
        onCancel={() => setIsOpenDeleteSet(false)}
      />
    </div>
  );
};

export default FlashCardSetDetail;
