import { Card, Rate } from "antd";
import { FC } from "react";
import { useNavigate } from "react-router";
interface IFlashCardSetInfoProps {
  id: number | string;
  setName: string;
  description?: string;
  numberOfTerms?: string | number;
  tagName?: string[];
  author: {
    name: string;
    avatar?: string;
  };
}
const FlashCardSetInfo: FC<IFlashCardSetInfoProps> = ({
  id,
  setName,
  numberOfTerms,
  description,
  tagName = [],
  author,
}) => {
  const navigate = useNavigate();
  return (
    <Card
      onClick={() => navigate(`/flashcard-set/${id}`)}
      className="cursor-pointer px-5 pt-3 pb-[50px] h-full font-bold shadow-lg border-b-2 border-b-purple hover:text-purple hover:scale-105"
    >
      <p className="text-lg on-2-line">{setName}</p>
      <div className="h-[50px]">
      <p className="on-2-line">{description}</p>
      </div>
      <p className="">{numberOfTerms || 0} Terms</p>
      <div className="flex gap-2 min-h-[20px]">
        {tagName.map((item, index) => (
          <span key={index} role="link" className="text-purple hover:underline">
            #{item}
          </span>
        ))}
      </div>
      <div className="flex justify-between items-center absolute bottom-3 left-5 right-5">
        <div className="flex items-center gap-2">
          <img
            width={30}
            src={author?.avatar}
            alt=""
            className="rounded-full"
          />
          <span className="">{author?.name}</span>
        </div>
        <Rate
          className="text-sm text-right"
          disabled
          allowHalf
          defaultValue={2.5}
        />
      </div>
    </Card>
  );
};

export default FlashCardSetInfo;
