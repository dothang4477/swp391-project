import {
  EditOutlined,
  FolderOutlined,
  PlusOutlined,
  ReadOutlined,
  ShareAltOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import { Col, Row, Divider, Popover, Button, Menu, Card } from "antd";
import { useNavigate } from "react-router-dom";
import {
  SettingOutlined,
  DeleteOutlined,
  IdcardOutlined,
} from "@ant-design/icons";
import { Dropdown } from "antd";
import FlashCardSetInfo from "components/FlashCardSet/CardInfo";

const FolderQuizfer = () => {
  const navigate = useNavigate();
  const items: number = 7;

  const userMenu = (
    <Menu>
      <Menu.Item key="1">
        <EditOutlined /> Edit
      </Menu.Item>
      <Menu.Item key="2">
        <div className="text-red-500">
          <DeleteOutlined /> Delete folder
        </div>
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Row className="flex w-full justify-between">
        <Col span={10} flex={3}>
          <div className="flex gap-2 items-center font-semibold text-[16px]">
            <p>(numberOfSet) 2</p>
            <Divider
              type="vertical"
              className="shadow-md border-indigo-500"
            ></Divider>
            <p>Created by</p>
            <img
              width={40}
              src={
                "https://i.pinimg.com/236x/1e/8e/88/1e8e888185e20a6b760534fff33b0229.jpg"
              }
              alt=""
              className="rounded-full "
            />
            <p>(userName)</p>
            <p className="border-none p-1 bg-slate-300 rounded-xl">(Role)</p>
          </div>
          <div className="flex gap-5 items-center">
            <FolderOutlined className="font-bold text-7xl" />
            <p className="font-bold text-4xl">(ClassName)</p>
          </div>
        </Col>
        <Col span={6} className="flex gap-10 justify-end items-center">
          <Popover placement="bottom" title="Add sets" arrowPointAtCenter>
            <Button>
              <PlusOutlined className="font-bold text-3xl" />
            </Button>
          </Popover>
          <Popover placement="bottom" title="Study" arrowPointAtCenter>
            <Button>
              <ReadOutlined className="font-bold text-3xl" />
            </Button>
          </Popover>
          <Popover placement="bottom" title="Share" arrowPointAtCenter>
            <Button>
              <ShareAltOutlined className="font-bold text-3xl" />
            </Button>
          </Popover>
          <Dropdown.Button
            style={{ float: "right" }}
            className="dropdown-btn"
            overlay={userMenu}
            icon={<SettingOutlined className="font-bold text-3xl" />}
          ></Dropdown.Button>
          <Button></Button>
        </Col>
      </Row>
      <Divider className="shadow-md border-indigo-400" />
      <Row className="flex gap-5 justify-around">
        {Array.from({ length: 10 }, (_, index) => (
          <Col span={5}>
            <FlashCardSetInfo
              key={index}
              id={1}
              setName={"Hieubeo"}
              description={"Hieudeptrai"}
              tagName={["deptrai", ""]}
              numberOfTerms={420}
              author={{
                name: "HieuBeo",
                avatar: "",
              }}
            />
          </Col>
        ))}
      </Row>
    </>
  );
};

export default FolderQuizfer;
