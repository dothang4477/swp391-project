import { Row } from "antd";
import MemberCardInfo from "components/ui/MemberCard";

export const Members = () => {
  return (
    <>
      <Row className="gap-10">
        <MemberCardInfo
          memberInfo={{ avatar: "", name: "Hieubeo", role: "Student" }}
        />
        <MemberCardInfo
          memberInfo={{ avatar: "", name: "Hieubeo", role: "Student" }}
        />
        <MemberCardInfo
          memberInfo={{ avatar: "", name: "Hieubeo", role: "Student" }}
        />
        <MemberCardInfo
          memberInfo={{ avatar: "", name: "Hieubeo", role: "Student" }}
        />
        <MemberCardInfo
          memberInfo={{ avatar: "", name: "Hieubeo", role: "Student" }}
        />
      </Row>
    </>
  );
};

export default Members;
