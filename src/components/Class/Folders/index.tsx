import FolderCardInfo from "components/ui/FolderCard";

export const Folders = () => {
  return (
    <>
      <FolderCardInfo
        folderInfo={{
          folderName: "HieudepTrai",
          author: { avatar: "" },
          flashcards: { total: 423 },
        }}
      />
    </>
  );
};

export default Folders;
