import {
  CopyOutlined,
  EditOutlined,
  EnvironmentOutlined,
  FolderAddOutlined,
  FolderViewOutlined,
  PlusOutlined,
  ReadOutlined,
  SnippetsOutlined,
  TeamOutlined,
  UserAddOutlined,
  UserOutlined,
  UsergroupDeleteOutlined,
} from "@ant-design/icons";
import { Col, Row, Tabs, Divider, Popover, Button, Menu } from "antd";
import ButtonQuizfer from "components/ui/button";
import { useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Sets from "./Sets";
import { SettingOutlined, DeleteOutlined } from "@ant-design/icons";
import Members from "./Members/index";
import { Dropdown } from "antd";
import Folders from "./Folders/index";

export const menuProfile = [
  {
    key: "Sets",
    tile: "Sets",
    icon: <SnippetsOutlined />,
  },
  {
    key: "Members",
    tile: "Members ",
    icon: <UserOutlined />,
  },
  {
    key: "Folders",
    tile: "Folders",
    icon: <FolderViewOutlined />,
  },
];

const ClassQuizfer = () => {
  const navigate = useNavigate();
  let { search } = useLocation();
  const query = new URLSearchParams(search);
  const [currentTab, setCurrentTab] = useState(query.get("tab") || "Sets");

  const renderContentByTab = useMemo(() => {
    switch (currentTab) {
      case "Sets":
        return (
          <>
            <Sets />
          </>
        );
      case "Members":
        return (
          <>
            <Members />
          </>
        );
      case "Folders":
        return (
          <>
            <Folders />
          </>
        );
    }
  }, [currentTab]);

  const userMenu = (
    <Menu>
      <Menu.Item key="1">
        <EditOutlined /> Edit
      </Menu.Item>
      <Menu.Item key="2">
        <div className="text-red-500">
          <DeleteOutlined /> Delete class
        </div>
      </Menu.Item>
      <Menu.Item key="3">
        <div className="text-red-500">
          <UsergroupDeleteOutlined /> Remove all members
        </div>
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <section>
        <Row className="flex w-full justify-between">
          <Col span={4} className="flex gap-5 items-center">
            <TeamOutlined className="font-bold text-7xl" />
            <p className="font-bold text-4xl">(ClassName)</p>
          </Col>
          <Col span={8} className="flex gap-10 justify-end items-center">
            <Popover placement="bottom" title="Add new set" arrowPointAtCenter>
              <Button>
                <PlusOutlined className="font-bold text-3xl" />
              </Button>
            </Popover>
            <Popover
              placement="bottom"
              title="Add new student"
              arrowPointAtCenter
            >
              <Button>
                <UserAddOutlined className="font-bold text-3xl" />
              </Button>
            </Popover>
            <Popover
              placement="bottom"
              title="Add new folder"
              arrowPointAtCenter
            >
              <Button>
                <FolderAddOutlined className="font-bold text-3xl" />
              </Button>
            </Popover>
            <Dropdown.Button
              style={{ float: "right" }}
              className="dropdown-btn"
              overlay={userMenu}
              icon={<SettingOutlined className="font-bold text-3xl" />}
            ></Dropdown.Button>
            <Button></Button>
          </Col>
        </Row>
        <Divider className="shadow-md border-indigo-400" />
        <div className="mt-5 grid col-span-2">
          <Row className="justify-around">
            <Col span={14}>
              <Tabs
                defaultActiveKey={currentTab}
                size="large"
                className="text-xl"
                onChange={(tab) => {
                  navigate(`?tab=${tab}`);
                  setCurrentTab(tab);
                }}
                items={menuProfile.map((item) => {
                  return {
                    label: (
                      <span className="text-lg">
                        {item.icon}
                        <span className="mb-0">{item.tile}</span>
                      </span>
                    ),
                    key: item.key,
                    children: renderContentByTab,
                  };
                })}
              />
            </Col>
            <Col span={6} className="flex flex-col items-end">
              <div className="flex flex-col gap-3 w-full">
                <p className="font-semibold text-xl text-slate-500">
                  Invitation link
                </p>
                <div className="flex items-center justify-between w-full">
                  <p className="text-lg">https://nhậu.vn/</p>{" "}
                  <ButtonQuizfer className="rounded-lg " color="purple">
                    <CopyOutlined className="text-xl" />
                  </ButtonQuizfer>
                </div>
                <p className="font-semibold text-xl text-slate-500">
                  Class Details
                </p>
                <div className="grid row-span-3">
                  <div>
                    <EnvironmentOutlined className="text-lg" /> (School)
                  </div>
                  <div className="mt-5">
                    <ReadOutlined className="text-lg" /> (1) set(s)
                  </div>
                  <div className="mt-5">
                    <UserOutlined className="text-lg" /> (1) member(s)
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </section>
    </>
  );
};

export default ClassQuizfer;
