import { Row, Col } from "antd";
import FlashCardSetInfo from "components/FlashCardSet/CardInfo";

export const Sets = () => {
  return (
    <>
      <Row className="flex gap-5 justify-around">
        {Array.from({ length: 10 }, (_, index) => (
          <Col span={11}>
            <FlashCardSetInfo
              key={index}
              id={1}
              setName={"Hieubeo"}
              description={"Hieudeptrai"}
              tagName={["deptrai", ""]}
              numberOfTerms={420}
              author={{
                name: "HieuBeo",
                avatar: "",
              }}
            />
          </Col>
        ))}
      </Row>
    </>
  );
};

export default Sets;
