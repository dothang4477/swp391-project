import React, { createContext, useState, useEffect } from "react";
import { useAppDispatch } from "reduxHook";
import { getProfile } from "store/user/userSlice";
interface AuthContextProps {
  isAuthenticated: boolean;
}

export const AuthContext = createContext<AuthContextProps>({
  isAuthenticated: false,
});

interface AuthContextProviderProps {
  children: React.ReactNode;
}

const AuthContextProvider: React.FC<AuthContextProviderProps> = ({
  children,
}) => {
  const dispatch = useAppDispatch();
  const [token, setToken] = useState<string | null>(
    localStorage.getItem("token") || null
  );
  const [isAuthenticated, setIsAuthenticated] = useState(!!token);

  useEffect(() => {
    if (token) {      
      dispatch(getProfile()).then(({ payload }) => {
        if (payload) {
          setIsAuthenticated(true);
          return;
        }
        localStorage.removeItem("token")
        setIsAuthenticated(false);
      });
    } else {
      setIsAuthenticated(false);
    }
  }, []);

  return (
    <AuthContext.Provider value={{ isAuthenticated }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;
