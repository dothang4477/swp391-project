import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { store } from "store";
import reportWebVitals from "reportWebVitals";
import Routing from "router/index";
import AuthContextProvider from "config/context/auth";
import { ConfigProvider, FloatButton } from "antd";
import NotificationProvider from "config/context/Notification";
import "./index.scss";
import "./antd.scss";
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';

const root = createRoot(document.getElementById("root") as HTMLElement);
root.render(
  // <React.StrictMode>
    <ConfigProvider
      theme={{
        token: {
          colorText:"#3e4a5c",
          colorTextBase: "#3e4a5c",
          fontFamily: `ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas,
          "Liberation Mono", "Courier New", monospace`,
        },
      }}
    >
      <NotificationProvider>
        <Provider store={store}>
          <AuthContextProvider>
            <Routing />
            <FloatButton.BackTop />
          </AuthContextProvider>
        </Provider>
      </NotificationProvider>
    </ConfigProvider>
  // </React.StrictMode>
);

reportWebVitals();
